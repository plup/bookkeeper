# Bookkeeper

## Deploy

Setup the project:
```
$ python -m venv bookkeeper/venv
$ . bookkeeper/venv/bin/activate
$ pip install wheel
$ pip install git+https://gitlab.com/happydevparis/bookkeeper.git
$ django-admin startproject website bookkeeper/
```

Configure `settings.py`:
```
X_FRAME_OPTIONS = 'SAMEORIGIN'
USE_L10N = False
DATE_FORMAT = 'Y-m-d'
DATE_INPUT_FORMATS = ('%Y-%m-%d', '%d/%m/%Y', '%d-%m-%Y')
```

Add routes in `urls.py`:
```
from django.urls import path, include
from django.shortcuts import redirect
urlpatterns = [
    path('', include('bookkeeper.urls')),
    path('', lambda request: redirect('budgets/', permanent=True)),
    # ...
]
```

## Develop

Run the app:
```
$ docker-compose up -d
```

Load test data:
```
$ docker-compose exec web python manage.py seed
```

Launch tests:
```
$ docker compose exec web python manage.py test bookkeeper
```
