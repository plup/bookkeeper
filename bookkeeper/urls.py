from django.urls import path
from . import views

urlpatterns = [
    # transactions
    path('transactions/', views.TransactionList.as_view(), name='transaction-list'),
    path('transactions/new/', views.TransactionCreate.as_view(), name='transaction-create'),
    path('transactions/<int:pk>/', views.TransactionDetail.as_view(), name='transaction-detail'),
    path('transactions/<int:pk>/edit/', views.TransactionEdit.as_view(), name='transaction-edit'),
    path('transactions/import/', views.TransactionImport.as_view(), name='transaction-import'),
    path('transactions/export/', views.TransactionExport.as_view(), name='transaction-export'),

    # transaction groups
    path('groups/new/', views.TransactionGroupCreate.as_view(), name='transaction-group-create'),
    path('groups/<int:pk>/', views.TransactionGroupDetail.as_view(), name='transaction-group-detail'),

    # accounts
    path('accounts/', views.AccountList.as_view(), name='account-list'),
    path('accounts/export/', views.AccountExport.as_view(), name='account-export'),

    # budgets
    path('budgets/', views.BudgetList.as_view(), name='budget-list'),
    path('budgets/new/', views.BudgetCreate.as_view(), name='budget-create'),
    path('budgets/<slug:code>/', views.BudgetDetail.as_view(), name='budget-detail'),
    path('budgets/<slug:code>/edit/', views.BudgetEdit.as_view(), name='budget-edit'),
]
