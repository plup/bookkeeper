from django.contrib import admin
from bookkeeper.models import Reconciliation


@admin.register(Reconciliation)
class RulesetAdmin(admin.ModelAdmin):
    pass
