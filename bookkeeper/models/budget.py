import logging
from decimal import Decimal, InvalidOperation
from datetime import datetime
from django.core.exceptions import ObjectDoesNotExist
from django.db import models, transaction
from django.db.models import DecimalField, Value, Case, When, Sum, Q, F
from django.db.models.functions import Replace, Cast, Round
from django.contrib.auth import get_user_model
from django.core.validators import RegexValidator
from django.utils.functional import cached_property
from bookkeeper.helpers import TransactionType
from bookkeeper.exceptions import *
from .account import Account
from .transaction import Transaction
from .transactiongroup import TransactionGroup

logger = logging.getLogger(__name__)


class BudgetQuerySet(models.QuerySet):
    """Built queries."""
    def search(self, **kwargs):
        """Return filtered objects."""
        if kwargs.get('supervisor'):
            self = self.filter(supervisor=kwargs['supervisor'])
        if kwargs.get('type'):
            self = self.filter(type=kwargs['type'])
        if kwargs.get('name'):
            self = self.filter(name__icontains=kwargs['name'])
        if not kwargs.get('close'):
            self = self.filter(status='O')
        return self

    def results(self, year=None, stack=False):
        """
        Calculate the operating result at the given year.

        The calculation produces the sames results than the Account.results()
        but hiting the database instead of the related manager makes a big
        change regarding performances.

        >>> timeit.timeit(stmt=Budget.objects.results, number=100)
        0.21987808999983827
        >>> timeit.timeit(stmt=Transaction.objects.all().get_accounts().results, number=100)
        6.29308167599811
        """
        if not year:
            year = datetime.today().year

        if stack:
            year_filter = Q(transactions__date__year__lte=year)
        else:
            year_filter = Q(transactions__date__year=year)

        return self.annotate(
            turnover = Sum('transactions__postings__amount', default=0,
                filter=(Q(transactions__postings__account__number__startswith='7')
                    & year_filter)
            ),
            expenses = Sum('transactions__postings__amount', default=0,
                filter=(Q(transactions__postings__account__number__startswith='6')
                    & year_filter)
            ),
            operating = F('turnover') + F('expenses'),
        )

    def status(self, year=None):
        """
        Calculate the objective to reach and the distance.

        This method is specific to business rules to make the calculation.

        As relative objective depends on the turnover at date, it calls the results()
        method to calculate the results first. The relative objective is calculated taking
        in account all transactions from budget start to date so it makes sense only when
        stacking the precedent years.
        """
        if not year:
            year = datetime.today().year

        return self.results(year, stack=True).annotate(
            # converted margin expected on budget (fixed or relative to turnover)
            objective_num = Case(
                When(objective__endswith=r'%',
                    then=Round(
                        Cast(
                            Replace('objective', Value('%')),
                            DecimalField(decimal_places=2, max_digits=10)
                        ) * Decimal('0.01') * F('turnover'),
                        precision=2
                    )
                ),
                # BUG: sideral side effect with the default when turnover is used in the when clause
                # replacing '%' again seems to workaround the casting issue
                default=Cast(
                    Replace('objective', Value('%')),
                    DecimalField(decimal_places=2, max_digits=10)
                ),
            ),

            # mark special product and expenses accounts
            exceptional = Sum('transactions__postings__amount', default=0,
                filter=(Q(transactions__postings__account__number__in=[6688, 7788])
                    & Q(transactions__date__year__lte=year))
            ),

            # engaged expenses are marked in a liability account
            # so its returned as a credit which makes sense with
            # availability returned as a credit as well
            engaged = Sum('transactions__postings__amount', default=0,
                filter=(Q(transactions__postings__account__number=408)
                    & Q(transactions__date__year__lte=year))
            ),

            # availability of funds is the difference between project operating result
            # with objective and exceptional loss/profit recorded in specifc accounts
            available = F('operating') - F('exceptional') - F('objective_num'),
        )


class BudgetManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().order_by('-number').status()

    def get_by_natural_key(self, code):
        """Use budget code when serialiazing data."""
        return self.get(code=code)

    def analytic(self, year=None, stack=False):
        """Return sums grouped by categories, sliced by year if given."""
        # build filters
        filter_products = Q(transactions__postings__account__number__startswith='7')
        filter_expenses = Q(transactions__postings__account__number__startswith='6')
        if year:
            if stack:
                filter_products &= Q(transactions__date__year__lte=year)
                filter_expenses &= Q(transactions__date__year__lte=year)
            else:
                filter_products &= Q(transactions__date__year=year)
                filter_expenses &= Q(transactions__date__year=year)

        # get the results grouped by budget type
        return self.values('type').annotate(
            turnover = Sum('transactions__postings__amount', default=0, filter=filter_products),
            expenses = Sum('transactions__postings__amount', default=0, filter=filter_expenses),
            operating = F('turnover') + F('expenses'),
        ).order_by()

    def analytic_totals(self, year=None, stack=False):
        return self.analytic(year, stack).aggregate(
            tot_turnover = Sum('turnover'),
            tot_expenses = Sum('expenses'),
            tot_operating = Sum('operating')
        )


class Budget(models.Model):
    """A grouping for transaction allowing analytic accounting."""
    class Meta:
        unique_together = ('type', 'number')

    type = models.CharField(max_length=20, choices=[
        ('G', 'General'),
        ('E', 'Events'),
        ('I', 'Investment'),
        ('P', 'Project'),
    ])
    status = models.CharField(default='O', max_length=5, choices=[
        ('O', 'Open'),
        ('C', 'Close'),
    ])
    objective = models.CharField(max_length=20, default='0', validators=[
        RegexValidator(regex='^\-?\d+(\.\d+)?\%?$', message='Must be a decimal or a percent')
    ])
    name = models.CharField(max_length=255)
    number = models.IntegerField()
    supervisor = models.ForeignKey(get_user_model(), blank=True, null=True, on_delete=models.PROTECT)
    # store the code to ease querying as we can't query calculated fields
    code = models.SlugField()

    objects = BudgetManager.from_queryset(BudgetQuerySet)()

    def __str__(self):
        return f'{self.code} - {self.name}'

    def natural_key(self):
        """Use budget code when deserialiazing data."""
        return (self.code,)

    def save(self, *args, **kwargs):
        """Autogenerate slug code from existing values."""
        if not self.number:
            last = Budget.objects.filter(type=self.type).order_by('number').last()
            if not last:
                self.number = 1
            else:
                self.number = last.number + 1
        self.code = self.type + str(self.number)
        return super().save(*args, **kwargs)

    def get_absolute_url(self):
        return f'/budgets/{self.code}/'

    @property
    def pending(self):
        """Return liability account not balanced."""
        #FIXME: replace by adding an annotation in the budget list view. It kills performances
        return self.transactions.get_accounts().filter(number__in=[401, 411]).exclude(balance=0)

    @property
    def to_distribute(self):
        """Unify the displaying of availability and engaged expenses."""
        return self.engaged if self.engaged else self.available

    def refresh(self, transaction=None):
        """
        Calculate the amount automatically engaged on a project.

        This method is specific to business rules to make the calculation.
        It marks all funds availabale projects as engaged. It updates a yearly
        transaction for expected expenses on projects.

        When triggered by a signal the triggering transaction is passed.
        """
        if self.type != 'P':
            return

        # start recalculation from the date of the given transaction or the fisrt
        # transaction on budget
        try:
            start_year = transaction.date.year
        except AttributeError:
            try:
                start_year = self.transactions.order_by('date').first().date.year
            except AttributeError:
                logger.info(f"Budget {self.code} is empty")
                return

        # recalculate per year (including current)
        for year in range(start_year, datetime.today().year+1):
            date = datetime(year, 12, 31)
            mark = f'AUTO FNP {year}'

            try:
                # delete the existing transactions
                try:
                    transaction = self.transactions.get(
                        automatic = True,
                        date = date,
                        payee = mark
                    )
                    transaction.group.transactions.all().delete()
                    transaction.group.delete()
                except ObjectDoesNotExist:
                    pass

                # recalculate availability at date
                # new engaged value is all that is available
                # availability is returned as credit but the engaged helper takes amount as debit
                engaged = -Budget.objects.filter(pk=self.pk).status(year=year).get().available

                if not engaged:
                    logger.info(f'Engaged expenses on {self.code} balanced in {year}')
                    continue # bypass creation of a transaction for this year

                # create the new engaged expenses transactions
                self.transactions.create_with_reverse(
                    automatic = True, # won't trigger refreshing again
                    group = TransactionGroup.objects.create(), # group them together
                    date = date,
                    payee = mark,
                    postings = TransactionType.engaged_expense(engaged)
                )

            except ProtectedTransactionException:
                logger.info(f"Can't update {self.code} for {year}")
                pass

        logger.info(f'Engaged expenses updated on {self.code} since {start_year}')
