"""This module contains all the logic about transactions and their components."""
import logging
from decimal import Decimal
from datetime import datetime
from django.db import models, IntegrityError, transaction
from django.db.models import Q, F, Count, Sum, Value, Case, When, Window
from django.db.models.functions import RowNumber
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from bookkeeper.exceptions import *
from .account import Account, AccountSetting
from .transactiongroup import TransactionGroup

logger = logging.getLogger(__name__)


class TransactionQuerySet(models.QuerySet):

    def delete(self):
        """Call object method to avoid bypassing integrity checks."""
        for obj in self:
            obj.delete()

    def search(self, **kwargs):
        """
        Transaction filtering.
        Proof filter only applies to records
        """
        query = Q()
        if kwargs.get('year'):
            query &= Q(date__year=kwargs['year'])
        if kwargs.get('budget'):
            query &= Q(budget__code=kwargs['budget'])
        if kwargs.get('search'):
            query &= (Q(payee__icontains=kwargs['search'])
                    | Q(evidence_ref__icontains=kwargs['search'])
                    | Q(id__startswith=kwargs['search']))
        if kwargs.get('record'):
            query &= Q(clearable=True)
        if kwargs.get('operation'):
            query &= Q(clearable=False)
        if kwargs.get('proof'):
            query &= Q(clearable=True, evidence='')

        # run the search
        self = self.filter(query).order_by('-date')

        # add the pending filter
        if kwargs.get('pending'):
            self = self.pending()

        return self

    def pending(self):
        """
        Return pending transactions with a different meaning for records and operations.

         * Operations are pending when writing in the temporary account.
         * Records are pending when existing outside of a group.
        """
        return self.alias(
            temporary = Count('postings', filter=Q(postings__account__number=471)),
            pending=Case(
                When(clearable=False, temporary__gt=0,
                    then=Value(True)
                ),
                When(clearable=True, group__isnull=True,
                     then=Value(True)
                ),
                default=Value(False)
            )
        ).filter(pending=True)

    def regroup(self):
        """View helper returning the transactions grouped with enumeration for each group."""
        return self.annotate(
                group_total = Window(
                    expression = Count('group_id'),
                    partition_by = F('group_id')
                ),
                group_number = Window(
                    expression = RowNumber(),
                    partition_by = F('group_id'),
                )
            ).order_by('-group_id', 'group_number')

    def get_accounts(self):
        """
        Syntaxic helper returning the accounts involved in the transactions
        with their balances.
        """
        if not self:
            return Account.objects.none()
        return Account.objects.balances(self).filter(balance__isnull=False)


class TransactionManager(models.Manager):

    @transaction.atomic
    def create(self, *args, postings=[], **kwargs):
        """
        Create the transaction and attaching a list of postings with account number and amount
        """
        trans = super().create(*args, **kwargs)
        trans.add_postings(postings)
        return trans

    @transaction.atomic
    def create_with_reverse(self, *args, **kwargs):
        """
        Create the transaction with the reverse transaction the next year.

        All parameters from the origin transactions are kept so both end up
        in the same group if any.
        """
        # create the transaction
        transaction = self.create(*args, **kwargs)
        # reverse the postings
        postings = [(p[0], -p[1]) for p in kwargs.get('postings')]
        # create the reverse transaction
        kwargs.update({
            'date': datetime(transaction.date.year+1, 1, 1),
            'postings': postings
        })
        reverse = self.create(*args, **kwargs)
        return transaction

    def get_queryset(self):
        """
        Add extra informations to help differentiate transaction types.

        Transactions types:

        Transaction are generally known to be either records (any documents asking to pay
        something like invoices or defrayal notes) or operations (when the money changes hands
        like bank transfers)
        A record (clearable transaction) should be cleared with an operation (not clearable).

        The transaction type is defined by its accounts reconciliation status. Accounts
        can be "reconciling" with `reconcile=True`, "reconcilable" with `reconcile=False`
        or none of it with reconcile=None (default).

        A clearable transaction writes in at least one reconciable account and not in any
        reconciling accounts. Other transactions are not clearable.

        About taxes:

        This method also tries to make sense to the amounts with/without taxes but those results
        are just guesses and are here to help the user understand what the transaction is about.
        /!\ The `tax_excl` and `tax_incl` are not suitable for internal calculation.

        A record (clearable transaction) generally writes in multiple products and expenses accounts.
        So summing those accounts is relevant for amount without tax.

        The same logic goes for taxes accounts so it gives us the amount with taxes.

        An operation generally writes in bank and in a liability account. It does not handle taxes
        but assests the final amount as been transfered. So only the amount in the reconciling 
        account matters.
        """
        return super().get_queryset().alias(
            # mark each account type
            accounts_reconciling = Count('postings', filter=Q(postings__account__reconcile=True)),
            accounts_reconcilable = Count('postings', filter=Q(postings__account__reconcile=False)),
        ).annotate(
            # decide who is clearable
            clearable = Case(
                When(accounts_reconciling=0,
                    accounts_reconcilable__gt=0,
                    then=Value(True)
                ),
                default=Value(False)
            )
        ).annotate(
            # return helpers regarding VAT depending of the transaction type
            tax_excl = Sum('postings__amount', default=0,
                filter=Q(postings__account__number__regex='^[6,7]')),
            tax_incl = Case(
                When(clearable=True,
                    then=F('tax_excl') + Sum('postings__amount', default=0,
                        filter=Q(postings__account__number__in=[4456,4457]))
                    ),
                default=Sum('postings__amount', default=0,
                    filter=Q(postings__account__reconcile=True)
                )
            )
        )


class Transaction(models.Model):
    """
    The main model for representing a financial event.

    Transactions link together many postings.

    A posting cannot exist on its own, it must have an equal and opposite
    posting (or set of postings) that completely balance out.
    """
    # relations
    accounts =  models.ManyToManyField('Account', through='Posting')
    budget = models.ForeignKey('Budget', null=True, blank=True, related_name='transactions', on_delete=models.PROTECT)
    group = models.ForeignKey('TransactionGroup', null=True, blank=True, related_name='transactions', on_delete=models.PROTECT)

    # fields
    date = models.DateField(help_text="Reference date.")
    payee = models.CharField(max_length=200, help_text="Name of the payee.")
    evidence = models.FileField(upload_to='proofs/', null=True, blank=True, help_text="Proof for the transaction.")
    evidence_ref = models.CharField(max_length=200, null=True, blank=True, help_text="Reference of the document.")
    evidence_internal = models.CharField(max_length=200, null=True, blank=True, help_text="Internal number.")
    notes = models.TextField(help_text="Any notes to go along with the transaction.", blank=True)
    automatic = models.BooleanField(default=False)

    objects = TransactionManager.from_queryset(TransactionQuerySet)()

    def __str__(self):
        return f"#{self.id} {self.date} {self.payee}"

    def get_absolute_url(self):
        return f'/transactions/{self.pk}/'

    def clean(self):
        """
        Check if the transaction balances and enforce the budget
        for non temporary transactions.

        Empty transactions are also allowed to not have budget because
        transactions are created before getting their types from the postings.
        """
        # check balance
        total = sum([entry.amount for entry in self.postings.all()])
        if total != 0:
            raise UnbalancedTransactionException(f"Difference between credits and debits: {abs(total)}")

        # check budget
        if (self.postings.exists()
                and not self.postings.filter(account__number=471).exists()
                and not self.budget):
            raise ValidationError('budget field cannot be null for this transaction.')

        # check accounts
        if self.postings.filter(account__reconcile=False).count() > 1:
            raise ValidationError('Writing in incompatible accounts')

        # check cutoff
        cutoff = AccountSetting.load().cutoff
        if cutoff and self.date.year <= cutoff:
            raise ProtectedTransactionException("Can't write a transaction in a year already cut off")

    def save(self, **kwargs):
        self.full_clean()
        super(Transaction, self).save(**kwargs)

    @transaction.atomic
    def delete(self, *args, force=False, **kwargs):

        # check reconciliation accounts
        if self.postings.filter(account__reconcile=True).exists() and not force:
            raise ProtectedTransactionException("The transaction writes in a reconciliation account and cannot be deleted.")

        # check cutoff
        cutoff = AccountSetting.load().cutoff
        if cutoff and self.date.year <= cutoff:
            raise ProtectedTransactionException("Can't delete a transaction from a year already cut off")
        self.postings.all().delete()
        return super().delete(*args, **kwargs)

    @transaction.atomic
    def add_postings(self, postings=[]):
        """
        Use the related manger to create postings from list of tuple.
        It accepts the account number or the object itself and
        keeps the transaction date to ease ordering in postings.
        """
        try:
            for p in postings:
                if p[1]:
                    try:
                        account = Account.objects.get(number=p[0])
                    except TypeError:
                        account = p[0]
                    self.postings.create(
                        account = account,
                        amount = p[1],
                    )

            self.clean() # raise Unbalance exception if needed
        except Account.DoesNotExist:
            raise InexistingAccountException('The transaction tries to write in a non existing account.')

    @transaction.atomic
    def promote(self, target_account, target_budget):
        """Promote an operation with temporary account to reconciled account."""
        try:
            temporary_posting = self.postings.get(account__number=471)
        except (ObjectDoesNotExist, MultipleObjectsReturned):
            raise InexistingAccountException("The transaction doesn't write in a temporary account")

        temporary_posting.account = target_account
        self.budget = target_budget
        temporary_posting.save()
        self.save() # trigger the signal after promotion

    @transaction.atomic
    def demote(self):
        """Return an operation to a temporary status."""
        try:
            promoted_posting = self.postings.get(account__reconcile=False)
        except (ObjectDoesNotExist, MultipleObjectsReturned):
            raise InexistingAccountException("Can't find the posting to demote.")

        promoted_posting.account = Account.objects.get(number=471)
        self.budget = None
        promoted_posting.save()
        self.save() # trigger the signal after promotion (useless because there is no budget)


class Posting(models.Model):
    """
    A single entry in a transaction.
    Postings are always part of a Transaction so that they balance according to double-entry bookkeeping.
    Amounts are kept as absolute values, negative values credit the related account and positive values debit the account.
    """

    # relations
    account = models.ForeignKey('Account', related_name='postings', on_delete=models.PROTECT)
    transaction = models.ForeignKey('Transaction', related_name='postings', on_delete=models.PROTECT)
    # fields
    amount = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return f"{self.amount}€ in {self.account}"

    def save(self, **kwargs):
        """Force precision."""
        # FIXME: raise error when trying to change a posting related to a cut off year
        # not mandatory as we never manipulate the postings out of a transaction context
        self.amount = Decimal(self.amount).quantize(Decimal('1.00'))
        super().save(**kwargs)


@receiver(post_save, sender=Transaction)
@receiver(post_delete, sender=Transaction)
def update_budget(sender, instance, **kwargs):
    """
    Refresh the budget after a transaction is saved and successfully committed.

    Doesn't trigger refreshing on automatic transactions to avoid recursion.
    """
    if instance.budget and not instance.automatic:
        transaction.on_commit(lambda: instance.budget.refresh(instance))
