from datetime import datetime
from django.db import models
from django.db.models import Q, Sum, F
from django.core.validators import MinValueValidator
from bookkeeper.exceptions import *


class AccountQuerySet(models.QuerySet):
    """Manage the account calculation."""
    def balances(self, transactions=None):
        """
        Return the accounts with a balance calculated on filtered transactions.
        Accounts with null balances were not involved in the transactions.
        """
        query = Q()
        if transactions:
            query = Q(postings__transaction__in=transactions.values('pk'))

        return self.annotate(balance=Sum('postings__amount', filter=query))

    def results(self):
        """Calculate operating result by summing balance of products and expenses."""
        return self.aggregate(
            turnover=Sum('balance', filter=Q(number__startswith='7'), default=0),
            expenses=Sum('balance', filter=Q(number__startswith='6'), default=0),
            operating=Sum('balance', filter=Q(number__regex=r'^[6,7]'), default=0),
        )


class AccountManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().balances().order_by('-number')


class Account(models.Model):
    """
    A group of postings all debiting or crediting the same resource.
    All accounts are of one of two types: either debits increase the value of an account or credits do.
    By convention, Asset and Expense accounts are of the former type, while Liabilities, Equity, and Revenue are of the latter.
    """
    class Types(models.TextChoices):
        AS = 'asset', 'Asset'
        LI = 'liability', 'Liability'
        IN = 'income', 'Income'
        EX = 'expense', 'Expense'
        EQ = 'equity', 'Equity'

    transactions =  models.ManyToManyField('Transaction', through='Posting')
    type = models.CharField(max_length=10, choices=Types.choices, help_text="Type of the account")
    name = models.CharField(unique=True, max_length=255, help_text="Name of this account")
    number = models.PositiveIntegerField(unique=True, help_text="Unique numeric identifier")
    description = models.TextField(blank=True, help_text="Usage.")
    reconcile = models.BooleanField(null=True, default=None, help_text="This account can be engaged in reconciliation. If False it should be reconciled. If True it can be used for reconciliation.")
    debit_increase = models.BooleanField(help_text="Mark how values are increased on the account")

    objects = AccountManager.from_queryset(AccountQuerySet)()

    def __str__(self):
        return f'{self.name} [{self.number}]'


class AccountSetting(models.Model):
    """Singleton model holding global accounting parameters."""
    cutoff = models.IntegerField(null=True, blank=True, validators=[MinValueValidator(1970)],
                                 help_text="Lock all transactions up to this year")

    def save(self, *args, **kwargs):
        self.pk = 1
        super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        pass

    @classmethod
    def load(cls):
        obj, _ = cls.objects.get_or_create(pk=1)
        return obj
