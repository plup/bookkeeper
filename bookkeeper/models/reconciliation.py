from django.db import models
from bookkeeper.exceptions import *


class Reconciliation(models.Model):
    """A single rule to help the reconciliation."""
    budget = models.ForeignKey('Budget', null=True, blank=True, related_name='rules', on_delete=models.SET_NULL)
    account = models.ForeignKey('Account', null=True, blank=True, related_name='rules', on_delete=models.SET_NULL)
    group = models.ForeignKey('TransactionGroup', null=True, blank=True, related_name='rules', on_delete=models.SET_NULL)
    string = models.CharField(max_length=100)

    def __str__(self):
        return self.string

