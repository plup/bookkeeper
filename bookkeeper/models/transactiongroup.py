from django.db import models
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.db import transaction
from bookkeeper.exceptions import *


class TransactionGroup(models.Model):
    """
    Result of the reconciling operation.
    """
    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return f'/groups/{self.pk}/'

    def is_valid(self):
        """
        Call all the validation methods including an optional
        transaction to be added to the group.
        """
        # empty groups are valid
        if not self.transactions.exists():
            return True

        self.check_budget()
        self.check_reconciliation_account()
        return True

    def check_budget(self):
        """Check all transactions are relative to the same budget."""
        from .budget import Budget
        try:
            return Budget.objects.filter(
                    transactions__in=self.transactions.values('pk')
                ).distinct().get()

        except MultipleObjectsReturned:
            raise AccountingValidationError("Those transactions are not writing in the same budget")

        except ObjectDoesNotExist:
            raise AccountingValidationError("No budget found")

    def check_reconciliation_account(self):
        """Check there is one and only one reconciliation account among all postings in the group."""
        try:
            return self.transactions.get_accounts().get(reconcile=False)

        except MultipleObjectsReturned:
            raise AccountingValidationError("Those transactions are writing in incompatible accounts")
        except ObjectDoesNotExist:
            raise AccountingValidationError("No reconciliation account found")

    def add_safe(self, trans):
        """
        Use this method instead of the add() from the related manager
        to add validation checks before adding transactions to groups.
        """
        # also overriding add() doesn't work through the related manager
        with transaction.atomic():
            trans.group = self
            trans.save()
            self.is_valid()

