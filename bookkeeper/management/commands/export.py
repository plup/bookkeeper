import csv
from datetime import datetime, date
from django.core.management.base import BaseCommand, CommandError
from bookkeeper.models import Transaction


class Command(BaseCommand):
    help = "Export data for external accounting"

    def add_arguments(self, parser):
        parser.add_argument('filename')
        parser.add_argument('--from', help='Starting date for the import. Defaults to last operation.')
        parser.add_argument('--to', help='Ending date for the import. Defaults to now.')

    def handle(self, *args, **options):
        """Manage operations."""

        # convert date
        try:
            start_date = datetime.strptime(options.get('from'), '%Y-%m-%d').date()
        except TypeError:
            start_date = date(1970, 1, 1)

        try:
            end_date = datetime.strptime(options.get('to'), '%Y-%m-%d').date()
        except TypeError:
            end_date = date.today()

        # Export data for cash accounting
        records = Transaction.objects.search(record=True).filter(date__lte=end_date,
                                                                 date__gte=start_date)

        with open(options.get('filename'), 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow([
                'Date',
                'Type',
                'Transaction number',
                'Group number',
                'Budget',
                'Evidence reference',
                'Payee',
                'Tax excl.',
                'Tax incl.',
                'Totally paid in current year'
            ])
            for record in records:
                # check if paid the current year
                if not record.group or record.group.transactions.search(operation=True).filter(
                            date__year__gt=end_date.year).exists():
                    paid = False
                else:
                    paid = True
                if record.postings.filter(account__number=411).exists():
                    type = 'Sale'
                elif record.postings.filter(account__number=401).exists():
                    type = 'Expense'
                else:
                    type = '?'
                writer.writerow([
                    record.date,
                    type,
                    record.id,
                    record.group.id if record.group else '',
                    record.budget.code,
                    record.evidence_ref,
                    record.payee,
                    record.tax_excl,
                    record.tax_incl,
                    paid
                ])
