from decimal import Decimal
from datetime import datetime
from django.core import management
from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q, Sum, Count, F
from bookkeeper.models import *


class Command(BaseCommand):
    help = 'Bookkeeping investigations'

    def add_arguments(self, parser):
        subparsers = parser.add_subparsers(title="subcommands", dest="subcommand", required=True)

        accounts = subparsers.add_parser('accounts')
        accounts.add_argument('--year', nargs='?', type=int, help="Filter on year")
        accounts.add_argument('--suspicious', action='store_true',
                help="Detect anomalies in accounts")

        budgets = subparsers.add_parser('budgets')
        budgets.add_argument('--year', nargs='?', type=int, help="Filter on year")
        budgets.add_argument('--empty', action='store_true', help="Find empty budgets")
        budgets.add_argument('--engaged', action='store_true',
            help='Engaged expenses')
        budgets.add_argument('--missing', action='store_true',
            help="Find missing engaged")
        budgets.add_argument('--refresh', action='store_true',
            help='Trigger execution of automatic refreshing')

        transactions = subparsers.add_parser('transactions')
        transactions.add_argument('--pending', action='store_true',
            help='Find pending transactions')
        transactions.add_argument('--missing', action='store_true',
            help="Find missing evidence")

    def handle(self, *args, **options):
        subcommand = options.pop('subcommand')
        if subcommand == 'accounts':
            return self.handle_accounts(**options)
        if subcommand == 'budgets':
            return self.handle_budgets(**options)
        if subcommand == 'transactions':
            return self.handle_transactions(**options)

    def handle_budgets(self, **options):
        """Run accounting rules checking."""
        year = options.get('year', datetime.today().year)

        # return the engaged expenses
        if options.get('engaged'):
            for budget in Budget.objects.all():
                try:
                    engaged = (budget.transactions.filter(date__year__lte=year)
                               .get_accounts().get(number=408).balance)
                    self.stdout.write(f'Engaged for {budget.code}: {round(engaged, 2)}')
                except Account.DoesNotExist:
                    pass

        if options.get('missing'):
            # check if budget with available have all expected expenses
            for b in Budget.objects.filter(type='P'):
                if b.available:
                    last_date = (b.transactions.filter(automatic=True)
                            .order_by('date').last().date.year)
                    if last_date < datetime.today().year:
                        self.stdout.write(self.style.WARNING(
                            f'{b.code} has {b.available} available and last automatic is in {last_date}'))

        if options.get('empty'):
            empty = (Budget.objects.annotate(Count('transactions'))
                    .filter(transactions__count=0)
                    .values('code', 'name'))
            for b in empty:
                self.stdout.write(f"{b['code']}: {b['name']}")

        if options.get('update'):
            for b in Budget.objects.all():
                try:
                    for year in range(
                        b.transactions.order_by('date').first().date.year,
                        datetime.today().year):
                        b.refresh(year)
                except AttributeError:
                    pass

    def handle_accounts(self, **options):
        """Handle all operations on accounts."""
        year = options.get('year', datetime.today().year)

        if options.get('suspicious'):

            # check unexpected balance in provider accounts
            provider_in_debit = (Account.objects
                .filter_transactions(year=year, stack=True)
                .filter(number=401,balance__gt=0))
            if provider_in_debit:
                self.stdout.write(self.style.ERROR(
                    f'Provider account is in debit for year {year}'
                ))

            # check unexpected balance in client accounts
            client_in_credit = (Account.objects
                .filter_transactions(year=year, stack=True)
                .filter(number=411,balance__lt=0))
            if client_in_credit:
                self.stdout.write(self.style.ERROR(
                    f'Client account is in credit for year {year}'
                ))

    def handle_transactions(self, **options):

        # check pending transactions
        if options.get('pending'):
            pendings = Transaction.objects.should_be_cleared()
            for transaction in pendings:
                self.stdout.write(self.style.WARNING(
                    f"Transaction #{transaction.id} from {transaction.date.year} in budget {transaction.budget.code} is pending"
                ))

        # check missing proof
        if options.get('missing'):
            missing = Transaction.objects.filter(clearable=True, evidence='')
            for transaction in missing:
                self.stdout.write(self.style.WARNING(
                    f"Transaction #{transaction.id} from {transaction.date.year} in budget {transaction.budget.code} is missing evidence"
                ))
