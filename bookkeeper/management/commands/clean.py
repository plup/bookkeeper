from django.core.management.base import BaseCommand, CommandError
from bookkeeper.models import TransactionGroup

class Command(BaseCommand):
    help = "Remove empty groups"

    def handle(self, *args, **options):
        """Manage operations."""
        for group in TransactionGroup.objects.all():
            if group.transactions.count() == 1:
                group.transactions.clear()
            if not group.transactions.exists():
                print(f'Removing group {group.id}...')
                group.delete()
