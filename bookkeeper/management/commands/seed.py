from django.core.management.base import BaseCommand, CommandError
from django.core.management import call_command
from datetime import datetime
from bookkeeper.models import TransactionGroup, Transaction, Budget, Account
from bookkeeper.forms import TransactionTransferForm
from bookkeeper.tests import factories

class Command(BaseCommand):
    help = 'Seed the storage with test data'

    def add_arguments(self, parser):
        parser.add_argument('--reset', action='store_true', help="Clear transactions and budgets")

    def handle(self, *args, **options):
        # clear objects
        if options.get('reset'):
            for transaction in Transaction.objects.all():
                transaction.delete(force=True)
            TransactionGroup.objects.all().delete()
            Budget.objects.all().delete()

        investment = factories.Investment()
        factories.SaleRecord.create_batch(3, budget=investment, amount=1000, vat=200)
        factories.PendingOperation(budget=investment, amount=3600)
        self.stdout.write(self.style.SUCCESS(f'Created an investment with matching pending operations in {investment.code}'))

        project = factories.Project()
        factories.SaleRecord(budget=project, date=datetime(2020, 8, 15), amount=1000, vat=200)
        factories.ExpenseRecord(budget=project, date=datetime(2021, 7, 10), amount=900, vat=180)
        self.stdout.write(self.style.SUCCESS(f'Created a balanced project in {project.code}'))

        project = factories.Project()
        factories.SaleRecord.create_batch(5, budget=project)
        factories.ExpenseRecord.create_batch(3, budget=project)
        factories.ExtraProfitRecord(budget=project)
        self.stdout.write(self.style.SUCCESS(f'Created a project with extra profit in {project.code}'))
        investment = factories.Investment()
        factories.ExpenseRecord.create_batch(10, budget=investment)
        factories.ExtraLossRecord(budget=investment)
        self.stdout.write(self.style.SUCCESS(f'Created an investment with extra loss in {investment.code}'))

        project1 = factories.Project()
        project2 = factories.Project()
        factories.SaleRecord.create_batch(5, budget=project1)
        TransactionTransferForm({
            "date": "2022-01-13",
            "payee": "me",
            "budget": project1.pk,
            "target": project2.pk,
            "account" : Account.objects.get(number=706),
            "amount": 1000,
        })
        self.stdout.write(self.style.SUCCESS(f'Created a transfert between {project1.code} and {project2.code}'))

        # run the reconciliation
        #call_command('reconcile')

