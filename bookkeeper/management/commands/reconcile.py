import logging
from django.core.management.base import BaseCommand, CommandError
from django.db.transaction import atomic
from bookkeeper.models import Reconciliation, Transaction, TransactionGroup
from bookkeeper.forms import TransactionGroupPromoteForm
from bookkeeper.exceptions import RollbackException

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    """Reconcile the pending operations with existing records."""
    help = 'Run the reconciliation mecanism'

    def add_arguments(self, parser):
        # dry is not implemented
        parser.add_argument('--dry', action='store_true', help="Don't commit changes to database.")

    def handle(self, *args, **options):
        """Call the reconciliation method."""
        dryrun = options.get('dry')
        self.reconcile_from_rules(dryrun)
        self.reconcile_from_reference(dryrun)

    def reconcile_from_reference(self, dryrun=False):
        """
        Affect a pending operation with a record matching on reference.
        Both transaction are associated in a new group.

        Going from pending records to pending operations because records provide
        more reliable informations whereas operation are imported from external
        sources (ex: reference is a field but from bank it's mixed in the payee)

        The matching is made on operations with a payee matching budget and reference
        (for sales the reference also contains the budget)
        """
        # FIXME: this method saves transaction multiple time triggering budget refresh each time
        for record in Transaction.objects.search(record=True, pending=True):
            # search for a matching operation based on budget and reference
            matches= Transaction.objects.search(operation=True, pending=True).filter(
                payee__iregex = f'(?<![0-9]){record.budget.code}(?![0-9a-z])'
            ).filter(
                payee__iregex = f'(?<![0-9]){record.evidence_ref}(?![0-9a-z])',
            )
            logger.debug(f'attempting to match record {record.id} on budget {record.budget.code} with evidence {record.evidence_ref}...')

            # associate the transactions
            if matches.exists():
                with atomic():
                    group = TransactionGroup.objects.create()
                    # put the record in the group so we can find the reconciliation account
                    group.add_safe(record)
                    account = group.check_reconciliation_account()
                    operation_ids = []
                    for match in matches:
                        match.promote(account, record.budget)
                        group.add_safe(match)
                        operation_ids.append(match.id)
                logger.info(f"record {record.id} matched with operations {operation_ids} in group {group.id}")
            else:
                logger.debug(f"record {record.id} not matched")

    def reconcile_from_rules(self, dryrun=False):
        """Affect the pending operations with the group from the rule."""
        for rule in Reconciliation.objects.all():
            for operation in Transaction.objects.search(operation=True, pending=True).filter(
                    payee__icontains = rule.string
                ):
                if rule.group:
                    # run the reconciliation process
                    form = TransactionGroupPromoteForm(instance=rule.group,
                                                       data={'promote-id': operation.id})
                    form.is_valid()
                    form.save()
                    logger.info(f"operation {operation.id} affected to group {rule.group.id} by rule {rule}")
                else:
                    # edit the transaction to turn it in a complete operation
                    operation.postings.filter(
                        account__number=471
                    ).update(
                        account=rule.account
                    )
                    operation.budget = rule.budget
                    operation.save()
                    logger.info(f"operation {operation.id} changed to account {rule.account} to by rule {rule}")
