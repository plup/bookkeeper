from django.core.management.base import BaseCommand, CommandError
from django.core.management.commands import loaddata
from django.db.models.signals import post_save, post_delete
from bookkeeper.models.transaction import Transaction, update_budget
from bookkeeper.models import Budget

class Command(loaddata.Command):
    """
    Wrapper around the loadata command.
    Disable the signals for the import to delay the automatic transaction calculation
    in the end of the import.
    """
    def handle(self, *args, **options):
        self.stdout.write('Disabling budget refresh signals...')
        post_save.disconnect(update_budget, sender=Transaction)
        post_delete.disconnect(update_budget, sender=Transaction)

        # run the command
        self.stdout.write('Loading data...')
        super(Command, self).handle(*args, **options)

        self.stdout.write('Reconnecting signals...')
        post_save.connect(update_budget, sender=Transaction)
        post_delete.connect(update_budget, sender=Transaction)

        # trigger recalculation for all projects
        self.stdout.write('Calculating budgets status...')
        for budget in Budget.objects.all():
            budget.refresh()
