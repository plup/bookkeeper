import factory
from base64 import b64decode
from django.contrib.auth.models import User as DjangoUser
from bookkeeper.models import Transaction as TransactionModel, Budget as BudgetModel
from bookkeeper.helpers import TransactionType

FAKE_PDF = b64decode("""
JVBERi0xLjMKMyAwIG9iago8PC9UeXBlIC9QYWdlCi9QYXJlbnQgMSAwIFIKL1Jlc291cmNlcyAy
IDAgUgovQ29udGVudHMgNCAwIFI+PgplbmRvYmoKNCAwIG9iago8PC9GaWx0ZXIgL0ZsYXRlRGVj
b2RlIC9MZW5ndGggMTk+PgpzdHJlYW0KeJwzUvDiMtAzNVco5wIAC/wCEgplbmRzdHJlYW0KZW5k
b2JqCjEgMCBvYmoKPDwvVHlwZSAvUGFnZXMKL0tpZHMgWzMgMCBSIF0KL0NvdW50IDEKL01lZGlh
Qm94IFswIDAgNTk1LjI4IDg0MS44OV0KPj4KZW5kb2JqCjIgMCBvYmoKPDwKL1Byb2NTZXQgWy9Q
REYgL1RleHQgL0ltYWdlQiAvSW1hZ2VDIC9JbWFnZUldCi9Gb250IDw8Cj4+Ci9YT2JqZWN0IDw8
Cj4+Cj4+CmVuZG9iago1IDAgb2JqCjw8Ci9Qcm9kdWNlciAoUHlGUERGIDEuNy4yIGh0dHA6Ly9w
eWZwZGYuZ29vZ2xlY29kZS5jb20vKQovQ3JlYXRpb25EYXRlIChEOjIwMjIwMzA4MTI1NzQ4KQo+
PgplbmRvYmoKNiAwIG9iago8PAovVHlwZSAvQ2F0YWxvZwovUGFnZXMgMSAwIFIKL09wZW5BY3Rp
b24gWzMgMCBSIC9GaXRIIG51bGxdCi9QYWdlTGF5b3V0IC9PbmVDb2x1bW4KPj4KZW5kb2JqCnhy
ZWYKMCA3CjAwMDAwMDAwMDAgNjU1MzUgZiAKMDAwMDAwMDE3NSAwMDAwMCBuIAowMDAwMDAwMjYy
IDAwMDAwIG4gCjAwMDAwMDAwMDkgMDAwMDAgbiAKMDAwMDAwMDA4NyAwMDAwMCBuIAowMDAwMDAw
MzU2IDAwMDAwIG4gCjAwMDAwMDA0NjUgMDAwMDAgbiAKdHJhaWxlcgo8PAovU2l6ZSA3Ci9Sb290
IDYgMCBSCi9JbmZvIDUgMCBSCj4+CnN0YXJ0eHJlZgo1NjgKJSVFT0YKQlQgL0YxIDE2LjAwIFRm
IEVUCkJUIDMxLjE5IDc5NC41NyBUZCAoRXZpZGVuY2UpIFRqIEVUCg==
""")

FAKE_CSV = bytes("""Date	Value date	Amount	Label	Balance
10/01/2019	13/01/2019	1102,08	VIR SOMETHING IMPORTANT	1102,08
15/03/2020	13/03/2020	14641,20	VIR F016-05	15743.28
20/06/2021	28/06/2021	-931,00	VIR P248 56	14812,28
25/09/2022	29/06/2022	-17,88	PRLV SEPA STR16NAO9CLDM7O9HCHJIRANANHQARIGJQ	14794,40
30/12/2023	30/06/2023	-354,00	PRLV SEPA EC-66492-CORE1691358	14440,40
""", "iso-8859-1")


class User(factory.django.DjangoModelFactory):
    class Meta:
        model = DjangoUser

    username = factory.Faker('first_name')


class Project(factory.django.DjangoModelFactory):
    class Meta:
        model = BudgetModel

    type = 'P'
    name = factory.Faker('sentence', nb_words=2)
    objective = '10%'
    supervisor = factory.SubFactory(User)


class Investment(factory.django.DjangoModelFactory):
    class Meta:
        model = BudgetModel

    type = 'I'
    name = factory.Faker('sentence', nb_words=2)
    objective = '-10000'
    supervisor = factory.SubFactory(User)


class Transaction(factory.django.DjangoModelFactory):
    class Meta:
        model = TransactionModel

    date = factory.Faker('date_between', start_date='-3y')
    payee = factory.Faker('company')
    budget = factory.SubFactory(Project)
    evidence = factory.django.FileField(filename='fake.pdf', data=FAKE_PDF)
    evidence_ref = factory.Faker('numerify', text='INV-###')


class ReverseTransaction(Transaction):
    """Use the specific manager method to create the 2 transactions."""
    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        manager = cls._get_manager(model_class)
        return manager.create_with_reverse(*args, **kwargs)


class HelperTransaction(Transaction):
    """Extends the base model transaction to use the transaction type helpers."""
    class Params:
        amount = factory.Faker('pydecimal', min_value=1000, max_value=2000, right_digits=2)
        vat = factory.Faker('pydecimal', min_value=100, max_value=200, right_digits=2)


class SaleRecord(HelperTransaction):
    """
    A sale record is an invoice we generate and send to the client.

    It provides a coherent evidence reference in order to work
    with the auto reconciliation mecanism.
    """
    class Meta:
        exclude = ('code', 'number')

    code = factory.SelfAttribute('budget.code')
    number = factory.Faker('numerify', text='###')
    evidence_ref = factory.LazyAttribute(
            lambda o: f'{o.code}-{o.number}'
        )
    postings = factory.LazyAttribute(
            lambda o: TransactionType.project_sale(amount=o.amount, vat=o.vat)
        )

class ExpenseRecord(HelperTransaction):
    """
    An expense record is an invoice we receive from contractors.

    It has a reference given by the contractor (can be anything).
    """
    evidence_ref = factory.Faker('bothify', text='??/###')
    postings = factory.LazyAttribute(
            lambda o: TransactionType.contractor_expense(amount=o.amount, vat=o.vat)
        )


class InvestmentRecord(ExpenseRecord):
    """An expense in a fixed objective budget."""
    budget = factory.SubFactory(Investment)


class PendingOperation(HelperTransaction):
    """
    A pending operation is a transaction without evidence or budget
    waiting for reconciliation.
    """
    evidence = None
    budget = None
    postings = factory.LazyAttribute(
            lambda o: TransactionType.bank_waiting(amount=o.amount)
        )


class SaleOperation(HelperTransaction):
    """A operation writing in the proper liaibility account after reconciliation."""
    # credit 411 and debit 512
    evidence = None
    postings = factory.LazyAttribute(
            lambda o: [(512, o.amount), (411, -o.amount)]
        )


class ExpenseOperation(HelperTransaction):
    """An operation writing in the proper liaibility account after reconciliation."""
    # debit 401 and credit 512
    evidence = None
    postings = factory.LazyAttribute(
            lambda o: [(512, -o.amount), (401, o.amount)]
        )


class ExtraProfitRecord(HelperTransaction):
    """An internal transaction recording an exceptional profit."""
    evidence = None
    postings = factory.LazyAttribute(
            lambda o: [(706, o.amount), (7788, -o.amount)]
        )


class ExtraLossRecord(HelperTransaction):
    """An internal transaction recording an exceptional loss."""
    evidence = None
    postings = factory.LazyAttribute(
            lambda o: [(604, -o.amount), (6688, o.amount)]
        )
