from django.test import TestCase
from bookkeeper.management.commands.reconcile import Command
from bookkeeper.models import TransactionGroup
from . import factories


class TestReference(TestCase):

    def test_reference_matched(self):
        """A sale operation with a reference number must match a record with same reference."""
        record = factories.SaleRecord(amount=1000, vat=200)
        waiting = factories.PendingOperation(amount=1200, payee=f'payment for {record.evidence_ref}')
        Command().reconcile_from_reference()
        # check group status
        group = TransactionGroup.objects.first()
        reconciliation_account = group.check_reconciliation_account()
        self.assertEqual(group.transactions.count(), 2)
        self.assertEqual(reconciliation_account.number, 411)
        self.assertEqual(reconciliation_account.balance, 0)

    def test_reference_no_match(self):
        """A sale operation with a proper reference must not change anything."""
        record = factories.SaleRecord(amount=1000, vat=200)
        waiting = factories.PendingOperation(amount=1200, payee=f'payment without reference')
        Command().reconcile_from_reference()
        # check no group
        self.assertFalse(TransactionGroup.objects.exists())

