"""Test the account calculations."""
from django.test import TestCase
from datetime import datetime
from bookkeeper.models import Account, Budget, Posting, Transaction, TransactionGroup
from . import factories

class TestBalances(TestCase):
    """Test the account balances calculation."""
    def setUp(self):
        self.group = TransactionGroup.objects.create()
        factories.Transaction(group=self.group, postings=[(512, 1000), (411, -1000)])
        factories.Transaction(group=self.group, postings=[(4456, -400), (4457, 400)])
        factories.Transaction(group=self.group, postings=[(512, 300), (611, -300)])
        factories.Transaction(postings=[(611, 17), (604, -33), (411, 16)])

    def test_balances(self):
        """Check global balances calculation."""
        accounts = Account.objects.all()
        self.assertEqual(accounts.get(number=411).balance, -984)
        self.assertEqual(accounts.get(number=512).balance, 1300)
        self.assertEqual(accounts.get(number=604).balance, -33)
        self.assertEqual(accounts.get(number=611).balance, -283)
        self.assertEqual(accounts.get(number=4456).balance, -400)
        self.assertEqual(accounts.get(number=4457).balance, 400)

    def test_balances_from_transaction_subset(self):
        """
        Check account balances calculated from transactions subset.
        """
        accounts = self.group.transactions.get_accounts()
        # check balances
        self.assertEqual(accounts.get(number=411).balance, -1000)
        self.assertEqual(accounts.get(number=512).balance, 1300)
        self.assertEqual(accounts.get(number=611).balance, -300)
        self.assertEqual(accounts.get(number=4456).balance, -400)
        self.assertEqual(accounts.get(number=4457).balance, 400)
        # check no other accounts are returned
        self.assertEqual(
            list(accounts.order_by('number').values_list('number', flat=True)),
            [411, 512, 611, 4456, 4457]
        )

    def test_empty_from_transaction_subset(self):
        """Check empty transaction subset doesn't return any accounts."""
        self.assertQuerysetEqual(Transaction.objects.none().get_accounts(), Account.objects.none())


class TestResults(TestCase):
    """Test the operating result calculation."""
    def test_operating(self):
        """Assert on the operating result at each step of a project."""
        # create an empty project
        budget = factories.Project()
        self.assertEqual(budget.transactions.get_accounts().results()['operating'], 0)

        # send an invoice to the client
        factories.SaleRecord(budget=budget, amount=1000, vat=200)
        # check result is 1000
        self.assertEqual(budget.transactions.get_accounts().results()['operating'], -1000)

        # the client transfer money to the bank
        factories.SaleOperation(budget=budget)
        # income statement didn't change
        self.assertEqual(budget.transactions.get_accounts().results()['operating'], -1000)

        # buy a service from contractor
        factories.ExpenseRecord(budget=budget, amount=900)
        # income statement is now 100
        self.assertEqual(budget.transactions.get_accounts().results()['operating'], -100)

        # we transfer money form the bank
        factories.ExpenseOperation(budget=budget, amount=900)
        # income statement didn't change
        self.assertEqual(budget.transactions.get_accounts().results()['operating'], -100)
