from django.test import TestCase
from bookkeeper.models import TransactionGroup
from bookkeeper.forms import *
from . import factories


class TestValidations(TestCase):
    """Test the group validation."""
    def setUp(self):
        self.group = TransactionGroup.objects.create()

    def test_empty(self):
        """Empty groups are valid."""
        self.assertTrue(self.group.is_valid())

    def test_different_budgets(self):
        """A group with transactions from different budgets is invalid."""
        factories.SaleRecord.create_batch(5, group=self.group)
        with self.assertRaises(AccountingValidationError):
            self.group.is_valid()

    def test_all_pending_operations(self):
        """A group of operations without budget or without reconciliation account is invalid."""
        factories.PendingOperation(group=self.group)
        with self.assertRaises(AccountingValidationError):
            self.group.is_valid()

    def test_sale_records_and_operations(self):
        """A group with only sale records and reconcilied sale operations is valid."""
        budget = factories.Project()
        factories.SaleRecord.create_batch(5, group=self.group, budget=budget)
        factories.SaleOperation.create_batch(3, group=self.group, budget=budget)
        self.assertTrue(self.group.is_valid())

    def test_expense_records_and_operations(self):
        """A group with only expense records and reconcilied expense operations is valid."""
        budget = factories.Project()
        factories.ExpenseRecord.create_batch(5, group=self.group, budget=budget)
        factories.ExpenseOperation.create_batch(3, group=self.group, budget=budget)
        self.assertTrue(self.group.is_valid())

    def test_sale_and_expense_records(self):
        """A group mixing sales and expenses records is invalid."""
        budget = factories.Project()
        factories.ExpenseRecord.create_batch(2, group=self.group, budget=budget)
        factories.SaleRecord.create_batch(2, group=self.group, budget=budget)
        with self.assertRaises(AccountingValidationError):
            self.group.is_valid()

    def test_add_safe_method(self):
        """A transaction making a group invalid must not be added through add_safe()."""
        factories.SaleRecord(group=self.group)
        t = factories.ExpenseRecord()
        with self.assertRaises(AccountingValidationError):
            self.group.add_safe(t)
        self.assertEqual(self.group.transactions.count(), 1)


class TestAddForm(TestCase):
    """Test adding a transaction into a group."""
    def setUp(self):
        self.group = TransactionGroup.objects.create()

    def test_add_record_to_empty(self):
        """Adding a record to an empty group must pass."""
        record = factories.SaleRecord()
        form = TransactionGroupAddForm(instance=self.group, data={'add-id': record.id})
        # check form
        self.assertTrue(form.is_valid())
        # check transaction in the group
        self.assertEqual(self.group.transactions.count(), 1)

    def test_add_different_budget(self):
        """Adding transaction with different budget must fail."""
        existing = factories.SaleRecord(group=self.group)
        adding = factories.SaleRecord()
        form = TransactionGroupAddForm(instance=self.group, data={'add-id': adding.id})
        # check form
        self.assertFalse(form.is_valid())
        self.assertIn('not writing in the same budget', form.errors.get('__all__')[0])

    def test_add_sale_to_sales(self):
        """Adding a sale to a group with a sale must pass."""
        existing = factories.SaleRecord(group=self.group)
        adding = factories.SaleRecord(budget=existing.budget)
        form = TransactionGroupAddForm(instance=self.group, data={'add-id': adding.id})
        # check form
        self.assertTrue(form.is_valid())
        # check transactions are part in the group
        self.assertEqual(self.group.transactions.count(), 2)

    def test_add_expense_to_expense(self):
        """Adding a expense to a group with an expense must pass."""
        existing = factories.ExpenseRecord(group=self.group)
        adding = factories.ExpenseRecord(budget=existing.budget)
        form = TransactionGroupAddForm(instance=self.group, data={'add-id': adding.id})
        # check form is ok
        self.assertTrue(form.is_valid())
        # check transactions are part in the group
        self.assertEqual(self.group.transactions.count(), 2)

    def test_add_expense_to_sales(self):
        """Adding an expense to a group with a sale must fail."""
        existing = factories.SaleRecord(group=self.group)
        adding = factories.ExpenseRecord(budget=existing.budget)
        form = TransactionGroupAddForm(instance=self.group, data={'add-id': adding.id})
        # check form is false
        self.assertFalse(form.is_valid())
        self.assertIn('incompatible accounts', form.errors.get('__all__')[0])
        # check there is still only one transaction in group
        self.assertEqual(self.group.transactions.count(), 1)


class TestPromoteForm(TestCase):
    def setUp(self):
        self.group = TransactionGroup.objects.create()

    def test_promote_operation_to_empty(self):
        """Adding a pending operation to an empty group must fail."""
        operation = factories.PendingOperation()
        form = TransactionGroupPromoteForm(instance=self.group, data={'promote-id': operation.id})
        # check form
        self.assertFalse(form.is_valid())
        self.assertIn('how to match', form.errors.get('__all__')[0])

    def test_promote_reconciled_operation(self):
        """Adding a reconciled operation must fail."""
        operation = factories.SaleOperation()
        form = TransactionGroupPromoteForm(instance=self.group, data={'promote-id': operation.id})
        # check form
        self.assertFalse(form.is_valid())
        self.assertIn('already reconciled', form.errors.get('__all__')[0])

    def test_complete_one_sale(self):
        """Performing the reconciliation with one sale must pass."""
        existing = factories.SaleRecord(group=self.group, amount=1000, vat=200)
        promoting = factories.PendingOperation(amount=1200)
        form = TransactionGroupPromoteForm(instance=self.group, data={'promote-id': promoting.id})
        self.assertTrue(form.is_valid())
        # check both transactions are in the group
        self.assertEqual(self.group.transactions.count(), 2)
        # check accounts 411 is null
        account = self.group.check_reconciliation_account()
        self.assertEqual(account.number, 411)
        self.assertEqual(account.balance, 0)


class TestDeleteForm(TestCase):
    def setUp(self):
        self.group = TransactionGroup.objects.create()

    def test_record(self):
        """Removing one record from a group must leave one pending record."""
        record = factories.SaleRecord(group=self.group)
        form = TransactionGroupDeleteForm(instance=self.group, data={'delete-id': record.id})
        # check form
        self.assertTrue(form.is_valid())
        # check the group and transaction
        self.assertFalse(self.group.transactions.exists())
        self.assertEqual(Transaction.objects.search(record=True, pending=True).count(), 1)

    def test_operation(self):
        """Removing an operation from a group must leave one pending operation."""
        promoted = factories.SaleOperation(group=self.group)
        form = TransactionGroupDeleteForm(instance=self.group, data={'delete-id': promoted.id})
        # check form
        self.assertTrue(form.is_valid())
        # check the group and transaction
        self.assertFalse(self.group.transactions.exists())
        self.assertEqual(Transaction.objects.search(operation=True, pending=True).count(), 1)
