"""Test the user navigation."""
from django.test import TestCase
from django.urls import reverse
from django.contrib.auth import get_user_model
from django.core.files.base import ContentFile
from bookkeeper.models import *
from . import factories


class TestBudget(TestCase):

    def setUp(self):
        get_user_model().objects.create_superuser('test')

    def test_display_list(self):
        r = self.client.get(reverse('budget-list'))
        self.assertEqual(r.status_code, 200)

    def test_create(self):
        # create a new budget
        r = self.client.post(reverse('budget-create'), data={
            'type': 'P',
            'name': 'my project',
            'objective': '5%',
            'supervisor': get_user_model().objects.first().pk
        })
        # assert redirect to the budget page
        self.assertEqual(r.status_code, 302)


class TestTransaction(TestCase):

    def test_display_list(self):
        r = self.client.get(reverse('transaction-list'))
        self.assertEqual(r.status_code, 200)

    def test_create_sale(self):

        budget = factories.Project()
        evidence = ContentFile(b'some pdf content', 'invoice')

        # create the sale transaction
        r = self.client.post(f"{reverse('transaction-create')}?type=sale", data={
            'budget': budget.pk,
            'date': '1970-01-01',
            'payee': 'me',
            'amount': 1000,
            'vat': 20,
            'evidence': evidence,
            'evidence_ref': 'ENV-100',
        })
        #print(r.context['form'].errors)

        # assert redirect to the transaction page
        self.assertRedirects(r, f'/transactions/{Transaction.objects.first().pk}/', status_code=302,
            target_status_code=200, fetch_redirect_response=True)

    def test_load_form_and_budget(self):

        from bookkeeper.forms import TransactionSaleForm
        budget = factories.Project()

        # get the form to create a new transaction on related budget
        r = self.client.get(f"{reverse('transaction-create')}?budget=P1&type=sale")
        self.assertEqual(r.context['form'].initial['budget'], budget.pk)
        self.assertTrue(isinstance(r.context['form'], TransactionSaleForm))
