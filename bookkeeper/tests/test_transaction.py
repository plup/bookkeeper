from django.test import TestCase
from unittest.mock import patch
from django.urls import reverse
from datetime import datetime
from decimal import Decimal
from django.db import IntegrityError
from django.core.files.uploadedfile import SimpleUploadedFile
from bookkeeper.models import *
from bookkeeper.forms import *
from bookkeeper.exceptions import *
from factory.django import mute_signals
from django.db.models.signals import post_save
from . import factories


class TestModel(TestCase):
    """
    Test the behavior of the transaction model.

    Signals are tested appart.
    """
    @mute_signals(post_save)
    def test_create_balanced(self):
        """Asserts a balanced transaction doesn't raise any error."""
        t = factories.Transaction(
            postings = [
                (411, 1200),
                (604, -1000),
                (4457, -400),
                (512, 200),
            ]
        )
        self.assertEqual(t.postings.count(), 4)

    @mute_signals(post_save)
    def test_create_reversed(self):
        """A transaction and its reversed transaction are opposite."""
        factories.ReverseTransaction(
            group = TransactionGroup.objects.create(),
            postings = [
                (411, 1000),
                (604, 500),
                (512, -1500),
            ]
        )
        # check amounts of the first posting in each
        self.assertEqual(Transaction.objects.first().postings.first().amount,
                         -Transaction.objects.last().postings.first().amount)

    def test_create_unbalanced(self):
        """Asserts a unbalaced transaction fails."""
        with self.assertRaises(UnbalancedTransactionException):
            factories.Transaction(
                postings=[
                    (411, 1200),
                    (512, -1000),
                ]
            )
        # assert no transaction is created
        self.assertFalse(Transaction.objects.exists())

    def test_create_no_budget(self):
        """A balanced transaction without budget must fail."""
        with self.assertRaises(ValidationError):
            factories.Transaction(
                budget = None,
                postings=[
                    (411, 1000),
                    (512, -1000),
                ]
            )
        # assert no transaction is created
        self.assertFalse(Transaction.objects.exists())

    def test_create_wrong_account(self):
        """An error with postings doesn't create any objects."""
        with self.assertRaises(InexistingAccountException):
            factories.Transaction(postings=[(512, 100), (1234, -100)])
        self.assertFalse(Transaction.objects.exists())

    def test_create_cutoff(self):
        """A transaction can't be written in a year alreay cutoff."""
        AccountSetting.objects.create(cutoff=2020)
        with self.assertRaises(ProtectedTransactionException):
            factories.SaleRecord(date=datetime(2020, 1, 1))

    def test_delete_operation(self):
        """A transaction writing in a reconciliation account can't be deleted."""
        t = factories.SaleOperation()
        # test model deletion
        with self.assertRaises(ProtectedTransactionException):
            t.delete()
        # assert transaction is still there with all postings
        self.assertEqual(t.postings.count(), 2)
        # test deletion through queryset
        with self.assertRaises(ProtectedTransactionException):
            Transaction.objects.all().delete()
        # assert transaction is still there with all postings
        self.assertEqual(t.postings.count(), 2)

    def test_delete_record(self):
        """A transaction not writing in a reconciliation account can be deleted."""
        t = factories.SaleRecord()
        t.delete()
        self.assertFalse(Transaction.objects.exists())
        # test delete with queryset
        factories.SaleRecord()
        Transaction.objects.all().delete()

    def test_delete_cutoff(self):
        """A transaction can't be deleted from a cutoff year."""
        t = factories.SaleRecord(date=datetime(2020, 1, 1))
        AccountSetting.objects.update(cutoff=2020)
        with self.assertRaises(ProtectedTransactionException):
            t.delete()

    @mute_signals(post_save)
    def test_default_only_yield_non_clearable(self):
        """Assert not reconciable/reconciling accounts produce a default transaction."""
        t = factories.Transaction(
            postings=[
                (622, 1000),
                (706, -1000),
            ]
        )
        # assert accounts are not reconciable or not reconciling (default)
        self.assertEquals(t.postings.filter(account__reconcile=None).count(), 2)
        # assert it produces a not clearable and not clearing transaction (default)
        self.assertTrue(Transaction.objects.filter(clearable=False).exists())

    @mute_signals(post_save)
    def test_reconcilable_and_default_yield_clearable(self):
        """
        Assert transaction writing in at least 1 reconciable account and not in any
        reconciling accunt produce a clearable transaction.
        """
        t = factories.Transaction(
            postings=[
                (411, 1000),
                (706, -1000),
            ]
        )
        # assert accounts match conditions
        self.assertTrue(t.postings.filter(account__reconcile=False).exists())
        self.assertTrue(t.postings.filter(account__reconcile=None).exists())
        # assert it produces a clearable transaction
        self.assertTrue(Transaction.objects.filter(clearable=True).exists())

    @mute_signals(post_save)
    def test_reconcilable_and_reconciling_yield_non_clearable(self):
        """
        Assert transaction writing in at least 1 reconciling account and 1 reconcilable
        account produce a default transaction.
        """
        t = factories.Transaction(
            postings=[
                (411, 1000),
                (512, -1000),
            ]
        )
        # assert accounts match conditions
        self.assertTrue(t.postings.filter(account__reconcile=True).exists())
        self.assertTrue(t.postings.filter(account__reconcile=False).exists())
        # assert it produces a not clearable transaction (default)
        self.assertTrue(Transaction.objects.filter(clearable=False).exists())

    @patch('bookkeeper.models.Budget.refresh')
    def test_signal(self, mock):
        """A non automatic transaction must trigger a signal after it's completed or deleted."""
        with self.captureOnCommitCallbacks(execute=True) as callbacks:
            factories.SaleRecord(automatic=False)
            factories.SaleRecord(automatic=True)
        self.assertEqual(len(callbacks), 1)

        with self.captureOnCommitCallbacks(execute=True) as callbacks:
            Transaction.objects.all().delete()
        self.assertEqual(len(callbacks), 1)

    @patch('bookkeeper.models.Budget.refresh')
    def test_promote_demote(self, mock):
        """."""
        budget = factories.Project()
        for number in [401, 411]:
            t = factories.PendingOperation()
            # check promotion
            with self.captureOnCommitCallbacks(execute=True) as callbacks:
                t.promote(Account.objects.get(number=number), budget)
            self.assertTrue(t.postings.filter(account__number=number).exists())
            self.assertTrue(t.budget)
            self.assertEqual(len(callbacks), 1)
            # check revert
            with self.captureOnCommitCallbacks(execute=True) as callbacks:
                t.demote()
            self.assertFalse(t.postings.filter(account__number=number).exists())
            self.assertFalse(t.budget)
            self.assertEqual(len(callbacks), 0) # no commit post_save


class TestManualForm(TestCase):
    """Test the implementation of the manual transaction create form."""
    def setUp(self):
        self.budget = factories.Project()
        self.files = {
            "evidence": SimpleUploadedFile('evidence.pdf', factories.FAKE_PDF,
                content_type='application/pdf')
        }
        self.data = {
            "date": "2022-01-13",
            "payee": "me",
            "budget": self.budget.pk,
            "evidence": self.files['evidence'],
            "evidence_ref": "EVI-000",
            "postings-TOTAL_FORMS": 3,
            "postings-INITIAL_FORMS": 0,
            "postings-0-account": 10,
            "postings-0-amount": 100,
            "postings-1-account": 11,
            "postings-1-amount": 50,
            "postings-2-account": 9,
            "postings-2-amount": -150,
        }

    def test_new_form(self):
        """Test the empty form creation."""
        TransactionManualForm()

    @patch('bookkeeper.models.Budget.refresh')
    def test_valid_postings(self, mock):
        """Test the form with valid postings."""
        # assert form valid
        form = TransactionManualForm(self.data, self.files)
        self.assertTrue(form.is_valid())
        # assert no error in formset
        self.assertFalse(form.formset.non_form_errors())
        self.assertFalse(form.formset.errors[0])
        self.assertFalse(form.non_field_errors())
        self.assertFalse(form.errors)
        # check objects created and signal called
        with self.captureOnCommitCallbacks(execute=True) as callbacks:
            form.save()
        self.assertEqual(Transaction.objects.count(), 1)
        self.assertEqual(Transaction.objects.first().postings.count(), 3)
        self.assertEqual(len(callbacks), 1)
        mock.assert_called_once()

    def test_invalid_postings(self):
        """A transaction with invalid postings fails."""
        data = self.data.update({
            "postings-0-account": 'error',
        })
        # assert form invalid
        form = TransactionManualForm(data)
        self.assertFalse(form.is_valid())

    def test_mismatch_error(self):
        """An error in the balance is returned from the formset."""
        self.data.update({
            "postings-0-amount": 0,
        })
        # check error is mismatch
        form = TransactionManualForm(self.data)
        self.assertFalse(form.is_valid())
        self.assertTrue('mismatch' in form.formset.non_form_errors()[0])

    @mute_signals(post_save)
    def test_null_amount(self):
        """A postings without amountis ignored from the transaction."""
        self.data.update({
            'postings-0-amount': 0,
            'postings-1-amount': 150,
        })
        form = TransactionManualForm(self.data, self.files)
        self.assertTrue(form.is_valid())
        form.save()
        # check objects
        self.assertEquals(Transaction.objects.first().postings.count(), 2)

    @mute_signals(post_save)
    def test_null_account(self):
        """A posting with invalid account is ignored from the transaction."""
        self.data.update({
            "postings-0-account": '',
            'postings-1-amount': 150,
        })
        form = TransactionManualForm(self.data, self.files)
        self.assertTrue(form.is_valid())
        form.save()
        # check objects
        self.assertEquals(Transaction.objects.first().postings.count(), 2)

    def test_empty_transaction(self):
        """A transaction without account fails."""
        self.data.update({
            "postings-0-account": '',
            "postings-1-account": '',
            "postings-2-account": '',
        })
        form = TransactionManualForm(data=self.data, files=self.files)
        self.assertFalse(form.is_valid())

    @patch('bookkeeper.models.Budget.refresh')
    def test_change_postings(self, mock):
        # create a transaction
        transaction = factories.SaleRecord()

        # change the amounts
        self.data.update({
            "payee": "you",
            "postings-TOTAL_FORMS": 3,
            "postings-INITIAL_FORMS": 0,
            "postings-0-account": 10,
            "postings-0-amount": 100,
            "postings-1-account": 11,
            "postings-1-amount": -100,
            "postings-2-account": 10,
            "postings-2-amount": 0, # triggers deletion
        })
        with self.captureOnCommitCallbacks(execute=True) as callbacks:
            new = TransactionManualForm(instance=transaction, data=self.data, files=self.files)
            self.assertTrue(new.is_valid())
            new.save()

        # check objects and signals
        self.assertEqual(Transaction.objects.count(), 1)
        self.assertEqual(len(callbacks), 1)
        # FIXME: for some reason the 3 old postings are still present when testing
        self.assertEqual(Transaction.objects.first().postings.count(), 5)


class TestSimpleForm(TestCase):
    """Test the implementation of the simple transaction form."""
    def setUp(self):
        self.budget = factories.Project()
        self.files = {
            "evidence": SimpleUploadedFile('evidence.pdf', factories.FAKE_PDF,
                content_type='application/pdf')
        }
        self.data = {
            "date": "2022-01-13",
            "payee": "me",
            "budget": self.budget.pk,
            "evidence": self.files['evidence'],
            "evidence_ref": "EVI-000",
            "amount": 1000,
            "vat": 200,
        }

    @patch('bookkeeper.models.Budget.refresh')
    def test_valid(self, mock):
        """Test the form with valid entries."""
        for _type in ['sale', 'outsourcing', 'general', 'defrayal']:
            form_class = TransactionRouterForm.get_form_class(_type)
            form = form_class(data=self.data, files=self.files)
            # check form
            self.assertTrue(form.is_valid())
            with self.captureOnCommitCallbacks(execute=True) as callbacks:
                form.save()
            # check objects and signals
            self.assertEqual(Transaction.objects.first().postings.count(), 3)
            self.assertEqual(len(callbacks), 1)
        self.assertEqual(mock.call_count, 4)


class TestTransferForm(TestCase):
    """Test the implementation of the transaction transfer form."""
    def setUp(self):
        self.origin = factories.Project()
        self.target = factories.Project()
        self.data = {
            "date": "2022-01-13",
            "payee": "me",
            "budget": self.origin.pk,
            "target": self.target.pk,
            "account" : Account.objects.get(number=611),
            "amount": 1000,
        }

    @patch('bookkeeper.models.Budget.refresh')
    def test_valid(self, mock):
        form = TransactionTransferForm(data=self.data)
        self.assertTrue(form.is_valid())
        with self.captureOnCommitCallbacks(execute=True) as callbacks:
            form.save()
        self.assertEqual(Transaction.objects.count(), 2)
        self.assertEqual(Account.objects.get(number=478).balance, 0)
        self.assertEqual(len(callbacks), 2)
        self.assertEqual(mock.call_count, 2)

    def test_same_budgets(self):
        self.data['target'] = self.origin.pk
        form = TransactionTransferForm(data=self.data)
        self.assertFalse(form.is_valid())


class TestImportForm(TestCase):
    """Test import functionality from file."""
    def setUp(self):
        self.csv = {
            "file": SimpleUploadedFile('import.csv', factories.FAKE_CSV,
                content_type='text/csv')
        }
        self.account = Account.objects.get(number=512)

    def test_empty_account(self):
        """Test the import when the account is empty."""
        form = TransactionImportForm(data={'account': self.account}, files=self.csv)
        # check the form validation
        self.assertTrue(form.is_valid())
        form.save()
        # check the result in db
        self.assertEqual(Transaction.objects.count(), 5)

    def test_non_empty_account(self):
        """Test the import when the account already has transactions."""
        # This also test the condition when existing and imported dates are equal
        factories.SaleOperation(date=datetime(2021, 6, 20))
        form = TransactionImportForm(data={'account': self.account}, files=self.csv)
        # check the form validation
        self.assertTrue(form.is_valid())
        form.save()
        # assert there is only 2 new transactions
        self.assertEqual(Transaction.objects.count(), 3)


@mute_signals(post_save)
class TestViews(TestCase):
    """Test the transaction create view routing mecanism."""
    def test_create_with_params(self):
        """Test parameters passed from the view to the form are honored."""
        budget = factories.Project()
        r = self.client.get(f"{reverse('transaction-create')}?type=manual&budget={budget.code}")
        self.assertEqual(r.context['form'].initial.get('budget'), budget.pk)

    def test_delete_operation(self):
        """Deleting an operation displays an error message."""
        operation = factories.PendingOperation()
        r = self.client.delete(reverse('transaction-detail', args=[operation.pk]))
        self.assertIn('cannot be deleted', str(r.getvalue()))
