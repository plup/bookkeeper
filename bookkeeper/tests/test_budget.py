from django.test import TestCase, TransactionTestCase
from django.db.models import signals
from unittest import skip
from unittest.mock import patch
from datetime import datetime
from decimal import Decimal
from bookkeeper.models import Budget, Transaction, TransactionGroup, Account, AccountSetting
from . import factories


class TestModel(TestCase):
    """Validate calculation of funds available."""
    def setUp(self):
        signals.post_save.disconnect(sender=Transaction)

    def test_objective_fixed(self):
        factories.Investment(objective='1000')
        self.assertEqual(Budget.objects.first().available, -1000)

    def test_objective_relative(self):
        budget = factories.Investment(objective='10%')
        self.assertEqual(Budget.objects.first().available, 0)
        factories.SaleRecord(budget=budget, amount=1000)
        self.assertEqual(Budget.objects.first().available, -900)

    def test_rounding(self):
        """Test a project with edge values for rounding."""
        budget = factories.Investment(objective='5%')
        factories.SaleRecord(budget=budget, amount=Decimal('1000.51'))
        self.assertEqual(Budget.objects.first().available, Decimal('-950.48'))


class TestStatus(TestCase):
    def setUp(self):
        signals.post_save.disconnect(sender=Transaction)

        self.project = factories.Project()
        factories.SaleRecord.create_batch(5, budget=self.project)
        factories.ExpenseRecord.create_batch(3, budget=self.project)
        factories.ExtraProfitRecord(budget=self.project)

        self.investment = factories.Investment()
        factories.ExpenseRecord.create_batch(10, budget=self.investment)
        factories.ExtraLossRecord(budget=self.investment)

    def test_results(self):
        """
        Ensure the results on the budgets are identical to the ones on the accounts.
        The budget results() is the base of the status() calculations.
        """
        for budget in Budget.objects.all():
            # check total results
            account_results = budget.transactions.get_accounts().results()
            self.assertEqual(account_results['operating'], budget.operating)

            # check yearly results
            year = budget.transactions.last().date.year
            account_results = budget.transactions.filter(date__year=year).get_accounts().results()
            budget_filtered = Budget.objects.filter(pk=budget.pk).results(year=year).get()
            self.assertEqual(account_results['operating'], budget_filtered.operating)

            # check yearly results stacked
            account_results = (budget.transactions.filter(date__year__lte=year)
                               .get_accounts().results())
            budget_filtered = (Budget.objects.filter(pk=budget.pk)
                               .results(year=year, stack=True).get())
            self.assertEqual(account_results['operating'], budget_filtered.operating)

    @skip('not implemented')
    def test_exceptionals(self):
        """Exceptional results are not taken in account for availability."""
        raise NotImplemented()

    @skip('not implemented')
    def test_engaged(self):
        raise NotImplemented()

    @skip('not implemented')
    def test_available(self):
        raise NotImplemented()


class TestRefresh(TransactionTestCase):
    """Test the refreshing mecanism run by signals on transactions."""
    serialized_rollback = True

    @patch('bookkeeper.models.budget.datetime', wraps=datetime)
    def test_engaged_created(self, mock):
        """When a sale in recorded in a project an unreceived expense is added."""
        # set today
        mock.today.return_value = datetime(1982, 3, 4)

        # create a sale record on a 10% project 2 years before
        factories.SaleRecord(date=datetime(1980, 8, 15), amount=1000)

        # check engaged per year
        engaged = Transaction.objects.filter(automatic=True)
        _1980 = engaged.filter(date__year=1980)
        self.assertEqual(_1980.count(), 1) # december only
        self.assertEqual(_1980.get_accounts().get(number=408).balance, -900)

        _1981 = engaged.filter(date__year=1981)
        self.assertEqual(_1981.count(), 2) # january and december
        self.assertEqual(_1981.get_accounts().get(number=408).balance, 0) # canceling each other

        _1982 = engaged.filter(date__year=1982)
        self.assertEqual(_1982.count(), 2) # january and december
        self.assertEqual(_1982.get_accounts().get(number=408).balance, 0) # canceling each other

        _1983 = engaged.filter(date__year=1983)
        self.assertEqual(_1983.count(), 1) # january only
        self.assertEqual(_1983.get_accounts().get(number=408).balance, 900)

    @patch('bookkeeper.models.budget.datetime', wraps=datetime)
    def test_engaged_updated(self, mock):
        """The engaged transaction is updated when project turnover changes."""
        # set today
        mock.today.return_value = datetime(1982, 3, 4)

        # create a sale record on a 10% project 2 years before today
        project = factories.Project()
        factories.SaleRecord(budget=project, date=datetime(1980, 8, 15), amount=1000)
        # add a sale record 1 year before today
        factories.SaleRecord(budget=project, date=datetime(1981, 7, 10), amount=2000)

        # there is still 2 engaged transactions per year + 2 for edge years
        # (see test_engaged_created for details)
        engaged = Transaction.objects.filter(automatic=True)
        self.assertEqual(engaged.count(), 6)

        # no extra group left (1 per year)
        self.assertEqual(TransactionGroup.objects.count(), 3)

        # check each year math
        self.assertEqual((engaged.filter(date__year=1980)
                          .get_accounts().get(number=408).balance), -900)
        self.assertEqual((engaged.filter(date__year=1981)
                          .get_accounts().get(number=408).balance), -1800)
        self.assertEqual((engaged.filter(date__year=1982)
                          .get_accounts().get(number=408).balance), 0)
        self.assertEqual((engaged.filter(date__year=1983)
                          .get_accounts().get(number=408).balance), 2700)

    @patch('bookkeeper.models.budget.datetime', wraps=datetime)
    def test_engaged_balanced(self, mock):
        """Engaged transaction is removed when project balances with objective"""
        # set today
        mock.today.return_value = datetime(1982, 3, 4)

        # create a sale record on a 10% project 2 years before
        project = factories.Project()
        factories.SaleRecord(budget=project, date=datetime(1980, 8, 15), amount=1000)
        # add an expense matching the 90%
        factories.ExpenseRecord(budget=project, date=datetime(1981, 7, 10), amount=900)
        # check each year math
        self.assertEqual((Transaction.objects.filter(date__year=1980)
                          .get_accounts().get(number=408).balance), -900)
        self.assertEqual((Transaction.objects.filter(date__year=1981)
                          .get_accounts().get(number=408).balance), 900)
        # there is no automatic transaction the year after the balancing
        self.assertFalse(Transaction.objects.filter(date__year=1982, automatic=True).exists())

    @patch('bookkeeper.models.budget.datetime', wraps=datetime)
    def test_engaged_propagated(self, mock):
        """
        The engaged expenses are propagated to next year if there is no transaction
        in the current year.
        Requires an explicit call to refresh() as there is no signal to trigger it.
        """
        # set today
        mock.today.return_value = datetime(1981, 1, 2)
        # create a sale record a years before
        project = factories.Project()
        factories.SaleRecord(budget=project, date=datetime(1980, 8, 15), amount=1000)
        # move in the future
        mock.today.return_value = datetime(1982, 4, 3)
        project.refresh()
        self.assertTrue(Transaction.objects.filter(date__year=1982, automatic=True).exists())


