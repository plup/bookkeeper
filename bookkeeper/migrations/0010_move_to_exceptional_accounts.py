# Generated by Django 4.0.2 on 2023-01-08 13:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookkeeper', '0009_delete_postingruleset'),
    ]

    def create_exceptional_accounts(apps, schema_editor):
        """
        Remove the custom accounts for exeptional restults and expenses
        in favor of the one given by the general accounting plan.
        """
        Account = apps.get_model('bookkeeper', 'Account')

        # rename accounts
        account = Account.objects.get(number=7061)
        account.number = 706
        account.name = 'prestations de services'
        account.description = 'ventes au client'
        account.save()

        account = Account.objects.get(number=7063)
        account.number = 7788
        account.name = 'produits exceptionnels divers'
        account.description = 'excédants sur projet'
        account.save()

        account = Account.objects.get(number=6041)
        account.number = 6688
        account.name = 'charges exceptionnelles diverses'
        account.description = 'pertes sur projet'
        account.save()

    operations = [
        migrations.RunPython(create_exceptional_accounts),
    ]
