from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bookkeeper', '0004_transactiongroup_transaction_group'),
    ]

    def set_default_types(apps, schema_editor):
        Account = apps.get_model('bookkeeper', 'Account')
        from bookkeeper.models import Account as Acc # load Types for reference
        Account.objects.filter(number__in=[401,455,4457,408]).update(type=Acc.Types.LI)
        Account.objects.filter(number__in=[411,4456,512]).update(type=Acc.Types.AS)
        Account.objects.filter(number__startswith='6').update(type=Acc.Types.EX)
        Account.objects.filter(number__startswith='7').update(type=Acc.Types.IN)

    operations = [
        # allow null to create the field
        migrations.AddField(
            model_name='account',
            name='type',
            field=models.CharField(choices=[('asset', 'Asset'), ('liability', 'Liability'), ('income', 'Income'), ('expense', 'Expense'), ('equity', 'Equity')], help_text='Type of the account', max_length=10, null=True),
        ),
        # set the custom defaults
        migrations.RunPython(set_default_types, migrations.RunPython.noop),
        # restore normal state
        migrations.AlterField(
            model_name='account',
            name='type',
            field=models.CharField(choices=[('asset', 'Asset'), ('liability', 'Liability'), ('income', 'Income'), ('expense', 'Expense'), ('equity', 'Equity')], help_text='Type of the account', max_length=10, null=False),
        ),
    ]

