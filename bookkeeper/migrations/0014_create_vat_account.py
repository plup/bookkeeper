# Generated by Django 4.0.2 on 2023-05-12 13:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookkeeper', '0013_remove_account_7062'),
    ]

    def create_accounts(apps, schema_editor):
        """Create the required accounts to track vat."""
        Account = apps.get_model('bookkeeper', 'Account')
        from bookkeeper.models import Account as Acc # load Types for reference

        ## Liabilities (=passif)
        # third party accounts (require reconciliation)
        Account.objects.create(name='tva à décaisser', number=44551, type=Acc.Types.LI,
                               description='tva à payer', debit_increase=False, reconcile=False)

        ## Assets (=actif)
        # third party accounts (require reconciliation)
        Account.objects.create(name='tva à reporter', number=44567, type=Acc.Types.AS,
                               description='tav à déduire', debit_increase=True, reconcile=False)
    operations = [
        migrations.RunPython(create_accounts),
    ]
