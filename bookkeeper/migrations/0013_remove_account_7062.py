from django.db import migrations
from bookkeeper.models import Transaction, Account, Budget


class Migration(migrations.Migration):

    dependencies = [
        ('bookkeeper', '0015_reconciliation'),
    ]

    def move_transactions(apps, schema_editor):
        """Move existing transactions to a specific budget."""
        try:
            temporary = Account.objects.get(number=478)
            bp = Budget.objects.get(code='I3')
            for transaction in Transaction.objects.filter(postings__account__number=7062):
                # change the account for the temporary account
                posting = transaction.postings.get(account__number=7062)
                posting.account = temporary
                posting.save()
                transaction.evidence_internal = transaction.evidence_ref
                transaction.evidence_ref = None
                transaction.save()
                # create the other transaction to complete the transfer
                transaction.pk = None
                transaction.budget = bp
                transaction.save()
                transaction.add_postings(
                    [(706, posting.amount), (478, -posting.amount)]
                )
                transaction.clean()
        except:
            pass

        Account.objects.get(number=7062).delete()

    operations = [
        migrations.RunPython(move_transactions),
    ]
