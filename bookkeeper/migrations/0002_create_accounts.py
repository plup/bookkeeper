from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookkeeper', '0001_initial'),
    ]

    def create_default_account(apps, schema_editor):
        """
        Create the basic accounts.

        By convention, asset and expense accounts increase the "value" of the account
        by debiting it, while liabilities, equity, and revenue by crediting it.
        This is only used for reporting.
        """
        Account = apps.get_model('bookkeeper', 'Account')

        ## Liabilities (=passif)
        # third party accounts (require reconciliation)
        Account.objects.create(name='fournisseurs', number=401, description='fournisseurs à payer',
            debit_increase=False, reconcile=False)
        Account.objects.create(name="compte courant d'associés", number=455,
            description='associés à rembourser', debit_increase=False, reconcile=False)
        # taxes accounts
        Account.objects.create(name='tva collectée', number=4457, description='tva collectée',
            debit_increase=False)
        # not recorded expenses (not involved in reconciliation)
        Account.objects.create(name="factures non parvenues", number=408,
            description='dépenses engagées', debit_increase=False)

        ## Assets (=actif)
        # third party accounts (require reconciliation)
        Account.objects.create(name='clients', number=411, description='paiements à encaisser',
            debit_increase=True, reconcile=False)
        # taxes accounts
        Account.objects.create(name='tva récupérable', number=4456, description='tva récupérable',
            debit_increase=True)
        # financial accounts (used for reconciliation)
        Account.objects.create(name='banques', number=512, description='variation de trésorerie',
            debit_increase=True, reconcile=True)

        ## Expenses (=charges)
        Account.objects.create(name='prestations de service', number=604,
            description='achats de prestation', debit_increase=True)
        Account.objects.create(name='excès de prestations', number=6041,
            description='pertes sur projets', debit_increase=True)
        Account.objects.create(name='sous-traitance', number=611,
            description='frais administratif', debit_increase=True)
        Account.objects.create(name='intermédiaires', number=622,
            description="achats d'apport d'affaire", debit_increase=True)
        Account.objects.create(name='missions', number=625,
            description='notes de frais', debit_increase=True)
        Account.objects.create(name='créances irrécouvrables', number=6714,
            description='pertes sur projets', debit_increase=True)
        Account.objects.create(name='services divers', number=628,
            description='frais divers', debit_increase=True)

        ## Revenue (=produit)
        Account.objects.create(name='projets numériques', number=7061,
            description='ventes de projets', debit_increase=False)
        Account.objects.create(name="apport d'affaire", number=7062,
            description="ventes d'apport d'affaire", debit_increase=False)
        Account.objects.create(name='excès de ventes', number=7063,
            description='excédants sur projets', debit_increase=False)

    operations = [
        migrations.RunPython(create_default_account),
    ]
