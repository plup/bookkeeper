from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    atomic = False

    dependencies = [
        ('bookkeeper', '0006_create_temporary_accounts'),
    ]

    def move_unbound_postings(apps, schema_editor):
        Account = apps.get_model('bookkeeper', 'Account')
        Transaction = apps.get_model('bookkeeper', 'Transaction')
        Posting = apps.get_model('bookkeeper', 'Posting')
        for posting in Posting.objects.filter(transaction__isnull=True):
            # create a new transaction from posting params
            transaction = Transaction.objects.create(
                date = posting.date,
                payee = posting.label or 'missing reference',
            )
            # affect the posting
            posting.transaction = transaction
            posting.save()
            # create the missing part
            Posting.objects.create(
                amount = -posting.amount,
                account = Account.objects.get(number=471),
                transaction = transaction
            )
            transaction.save() # validate

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='budget',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='transactions', to='bookkeeper.budget'),
        ),
        migrations.RunPython(move_unbound_postings),
        migrations.RemoveField(
            model_name='posting',
            name='date',
        ),
        migrations.RemoveField(
            model_name='posting',
            name='label',
        ),
        migrations.AlterField(
            model_name='posting',
            name='transaction',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='postings', to='bookkeeper.transaction'),
        ),
        migrations.AddField(
            model_name='account',
            name='transactions',
            field=models.ManyToManyField(through='bookkeeper.Posting', to='bookkeeper.Transaction'),
        ),
    ]
