from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookkeeper', '0005_account_type'),
    ]

    def create_temporary_accounts(apps, schema_editor):
        """
        Create temporaty accounts.
        Those accounts are marked as liabilities but they can hold different types of transactions.
        It doesn't really matter because they should be empty or null balanced when validating
        accounts.
        """
        Account = apps.get_model('bookkeeper', 'Account')
        from bookkeeper.models import Account as Acc # load Types for reference
        Account.objects.create(name="compte d'attente", number=471, type=Acc.Types.LI,
            description='en attente de rapprochement', debit_increase=False)
        Account.objects.create(name="compte transitoire", number=478, type=Acc.Types.LI,
            description='mouvements internes', debit_increase=False)

    operations = [
        migrations.RunPython(create_temporary_accounts),
    ]
