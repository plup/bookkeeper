from django.db import migrations


class Migration(migrations.Migration):
    atomic = False

    dependencies = [
        ('bookkeeper', '0010_move_to_exceptional_accounts'),
    ]

    def migrate_to_groups(apps, schema_editor):
        Transaction = apps.get_model('bookkeeper', 'Transaction')
        TransactionGroup = apps.get_model('bookkeeper', 'TransactionGroup')
        for transaction in Transaction.objects.filter(clears__isnull=False):
            transaction.refresh_from_db()
            if not transaction.group:
                # put transations in a new group
                group = TransactionGroup.objects.create()
                transaction.group = group
                transaction.clears.group = group
                transaction.save()
                transaction.clears.save()

    operations = [
        migrations.RunPython(migrate_to_groups),
        migrations.RemoveField(
            model_name='transaction',
            name='clears',
        ),
    ]
