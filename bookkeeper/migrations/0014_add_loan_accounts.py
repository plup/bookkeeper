from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('bookkeeper', '0012_accountsetting'),
    ]

    def create_loan_accounts(apps, schema_editor):
        """Create the required accounts to track loans."""
        Account = apps.get_model('bookkeeper', 'Account')
        from bookkeeper.models import Account as Acc # load Types for reference

        ## Liabilities (=passif)
        # third party accounts (require reconciliation)
        Account.objects.create(name='emprunts', number=164, type=Acc.Types.LI,
                               description='capital restant', debit_increase=False, reconcile=False)

        ## Expenses (=charges)
        Account.objects.create(name='assurances', number=616, type=Acc.Types.EX,
                               description='assurances bancaires', debit_increase=True)
        Account.objects.create(name="charges d'intérêts", number=6611, type=Acc.Types.EX,
                               description="intérêts d'emprunts", debit_increase=True)

    operations = [
        migrations.RunPython(create_loan_accounts),
    ]
