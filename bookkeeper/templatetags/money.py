from enum import Enum
from decimal import Decimal
from django.utils.safestring import mark_safe
from django import template
register = template.Library()

@register.filter(name='money')
def money(value, colored=True):
    """Format the value for display according to the type of accounts it works with."""

    if not (isinstance(value, Decimal) or isinstance(value, float)
            or isinstance(value, int)):
        return '-'

    # fix for sqlite which doesn't support decimals
    value = round(value, 2)

    if not colored:
        return mark_safe(f'{value:,}€'.replace(',','&#8239;'))

    if value <= 0:
        return mark_safe(f'<span style="color: green;">⇑&#8239;{-value:,}€</span>'.replace(',','&#8239;'))
    else:
        return mark_safe(f'<span style="color: red;">⇓&#8239;{value:,}€</span>'.replace(',','&#8239;'))
