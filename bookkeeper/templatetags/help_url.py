from django import template
from django.conf import settings
register = template.Library()

@register.simple_tag()
def help_url():
    try:
        return settings.HELP_URL
    except AttributeError:
        return ''

