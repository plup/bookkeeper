import csv
from django.views.generic import DetailView, ListView
from django.http import HttpResponse
from bookkeeper.forms import AccountFilterForm
from bookkeeper.models import Account, Budget, Transaction, Posting
from .mixins import HTMXTemplateMixin


class AccountList(HTMXTemplateMixin, ListView):
    """Bind the general accounting with the budget analytics."""
    model = Account
    context_object_name = 'accounts'

    def get_context_data(self, **kwargs):
        """Render extra content."""
        context = super().get_context_data(**kwargs)
        # display the filter form
        context['filter_form'] = AccountFilterForm(self.request.GET)
        # get filter params
        stack = self.request.GET.get('stack', False)
        try:
            year = int(self.request.GET.get('year'))
        except (ValueError, TypeError):
            year = None
        # add budget analytics
        context['analytic'] = Budget.objects.analytic(year, stack)
        context['analytic_totals'] = Budget.objects.analytic_totals(year, stack)

        return context

    def get_queryset(self):
        """Return the accounts calculated on the filtered transactions."""
        qs = super().get_queryset()
        form = AccountFilterForm(self.request.GET)
        if form.is_valid():
            year = form.cleaned_data['year']
            if not year:
                return qs
            if form.cleaned_data['stack']:
                transactions = Transaction.objects.filter(date__year__lte=year)
            else:
                transactions = Transaction.objects.filter(date__year=year)
            qs = qs.balances(transactions=transactions)
        return qs


class AccountExport(AccountList):
    """Export all postings in accounts into CSV."""
    def render_to_response(self, context, **kwargs):
        """
        Overide the rendering method to return CSV.
        Return all the postings in all accounts on the filtered transactions.
        """
        # filter transactions to export
        form = AccountFilterForm(self.request.GET)
        postings = Posting.objects.all()
        if form.is_valid():
            year = form.cleaned_data['year']
            if year:
                if form.cleaned_data['stack']:
                    postings = postings.filter(transaction__date__year__lte=year)
                else:
                    postings = postings.filter(transaction__date__year=year)

        # build the response
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="accounts.csv"'
        writer = csv.writer(response)
        writer.writerow([
            'Date',
            'Account number',
            'Account name',
            'Transaction number',
            'Group number',
            'Evidence reference',
            'Payee',
            'Amount'
        ])

        # return all postings from the transactions
        for posting in postings:
            writer.writerow([
                posting.transaction.date,
                posting.account.number,
                posting.account.name,
                posting.transaction.id,
                posting.transaction.group.id if posting.transaction.group else '',
                posting.transaction.evidence_ref,
                posting.transaction.payee,
                posting.amount,
            ])
        return response
