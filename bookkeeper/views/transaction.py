import csv
from operator import itemgetter
from itertools import groupby
from django.core.management import call_command
from django.views.generic import View, FormView, CreateView, DetailView, ListView, UpdateView, TemplateView, RedirectView

from django.urls import reverse, reverse_lazy
from django.shortcuts import redirect
from django.http import HttpResponse, QueryDict
from bookkeeper.forms import *
from bookkeeper.models import Transaction, Account, Posting, Budget, TransactionGroup
from .mixins import HTMXTemplateMixin, HttpResponseHXError, HttpResponseHXRedirect


class TransactionList(HTMXTemplateMixin, ListView):
    """
    Entrypoint for transactions.
    This view renders the result table with a filtering form.
    """
    model = Transaction
    context_object_name = 'transactions'
    paginate_by = 100

    def get_queryset(self):
        """Return a list of transactions filtered with the form."""
        qs = super().get_queryset().order_by('-date')
        # get results
        self.form = TransactionSearchForm(self.request.GET or None)
        if self.form.is_valid():
            qs = qs.search(**self.form.cleaned_data)
            # regroup for displaying
            if self.form.cleaned_data.get('group'):
                qs = qs.regroup()
        return qs

    def get_context_data(self, **kwargs):
        """Add the filter form."""
        context = super().get_context_data(**kwargs)
        context['filter_form'] = self.form
        return context


class TransactionImport(HTMXTemplateMixin, FormView):
    """Load a transaction list from a CSV."""
    form_class = TransactionImportForm
    template_name = 'bookkeeper/transaction_import.html'

    def form_valid(self, form):
        try:
            first = form.cleaned_data['transactions'][0]['date']
            count = len(form.cleaned_data['transactions'])
            details = f'{count} new transactions since the {first}'

        except IndexError:
            return HttpResponse('No new transaction')

        if 'save' in self.request.POST.keys():
            # creates the transactions
            form.save()
            # run a reconciliation
            call_command('reconcile')
            return HttpResponse(f'Imported {details}')
        else:
            return HttpResponse(f'This file contains {details}')

    def form_invalid(self, form):
        return HttpResponseHXError(str(form.errors))



class TransactionExport(TransactionList):
    """Turn the transaction list into CSV."""
    def render_to_response(self, context, **kwargs):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="transactions.csv"'
        fields = ['date', 'evidence_ref', 'budget__code', 'tax_excl', 'tax_incl', 'payee']
        transactions = self.get_queryset().values(*fields)
        writer = csv.DictWriter(response, fields)
        writer.writeheader()
        writer.writerows(transactions)
        return response


class TransactionDetail(DetailView):
    """Display the transaction details."""
    model = Transaction
    context_object_name = 'transaction'
    fields = ('tag', 'payee', 'notes', 'clears', 'evidence')

    def delete(self, *args, **kwargs):
        """Delete the transaction and make a client redirect to the list."""
        try:
            self.get_object().delete()
            return HttpResponseHXRedirect(redirect_to=reverse_lazy('transaction-list'))
        except ProtectedTransactionException as e:
            return HttpResponseHXError(str(e))


class TransactionCreate(HTMXTemplateMixin, CreateView):
    model = Transaction

    def get_context_data(self, **kwargs):
        """Add the transaction helper to select the creation form."""
        context = super().get_context_data(**kwargs)
        context['router_form'] = TransactionRouterForm(self.request.GET)
        return context

    def get_form_class(self):
        """Instanciate a form according to the selected type."""
        _type = self.request.GET.get('type')
        return TransactionRouterForm.get_form_class(_type)

    def get_form_kwargs(self):
        """Set initial value for budget."""
        kwargs = super().get_form_kwargs()
        budget = self.request.GET.get('budget')
        if budget:
            kwargs['initial']['budget'] = Budget.objects.get(code=budget).pk
        return kwargs

    def get_success_url(self):
        return reverse('transaction-detail', kwargs={'pk' : self.object.pk})


class TransactionEdit(UpdateView):
    """
    Update the transaction details.
    Uses the same template and form than the CreateView.
    Adds extra method to handle the DELETE with htmx.
    """
    model = Transaction
    context_object_name = 'transaction'
    form_class = TransactionManualForm

    def get_success_url(self):
        return reverse('transaction-detail', kwargs={'pk' : self.object.pk})


