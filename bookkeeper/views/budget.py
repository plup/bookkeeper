from django.views.generic import FormView, CreateView, DetailView, ListView, UpdateView
from django.urls import reverse
from django.http import HttpResponse
from bookkeeper.models import Budget, Account
from bookkeeper.forms import BudgetCreateForm, BudgetEditForm, BudgetFilterForm
from .mixins import HTMXTemplateMixin


class BudgetList(HTMXTemplateMixin, ListView):
    """View with combing Form with List."""
    model = Budget
    context_object_name = 'budgets'

    def get_context_data(self, **kwargs):
        """Add the budget list and filters."""
        context = super().get_context_data(**kwargs)
        context['filters'] = BudgetFilterForm(self.request.GET)
        return context

    def get_queryset(self):
        """Return a filtered list of budgets."""
        qs = super().get_queryset()
        filters = BudgetFilterForm(self.request.GET)
        if filters.is_valid():
            return qs.search(**filters.cleaned_data)
        # run an empty search on the queryset to filter out close budget when loading
        return qs.search()


class BudgetDetail(DetailView):
    """
    Update the transaction details.
    Uses the same template and form than the CreateView.
    """
    model = Budget
    context_object_name = 'budget'
    template_name_suffix = '_detail'
    slug_field = 'code'
    slug_url_kwarg = 'code'

    def get_context_data(self, **kwargs):
        """Feed the templates."""
        context = super().get_context_data(**kwargs)
        accounts = self.object.transactions.get_accounts()
        # add account details
        context['accounts'] = accounts
        context['liabilities'] = list(accounts.filter(number__in=[401, 411])
                                      .exclude(balance=0).values_list('name', flat=True))
        # add last transactions
        context['transactions'] = self.object.transactions.order_by('-date')[:10]
        return context


class BudgetCreate(CreateView):
    """Create a budget from a modal."""
    model = Budget
    context_object_name = 'budget'
    template_name = 'bookkeeper/fragments/budget_edit.html'
    form_class = BudgetCreateForm


class BudgetEdit(UpdateView):
    """Update a budget from a modal."""
    model = Budget
    context_object_name = 'budget'
    template_name = 'bookkeeper/fragments/budget_edit.html'
    form_class = BudgetEditForm
    slug_field = 'code'
    slug_url_kwarg = 'code'
