from django.views.generic import View, FormView, CreateView, DetailView, ListView, UpdateView, TemplateView, RedirectView
from bookkeeper.forms import *
from bookkeeper.models import Transaction, Account, Posting, Budget, TransactionGroup
from bookkeeper.exceptions import *
from .mixins import HTMXTemplateMixin, HttpResponseHXError, HttpResponseHXRedirect


class TransactionGroupCreate(View):
    """Associate the given transaction in a new group and redirect."""
    def post(self, *args, **kwargs):
        try:
            transaction = Transaction.objects.get(pk=self.request.POST.get('id'),
                                                  group__isnull=True)
            group = TransactionGroup.objects.create()
            group.add_safe(transaction)
            return HttpResponseHXRedirect(group.get_absolute_url())
        except AccountingValidationError as e:
            return HttpResponseHXError(str(e))


class TransactionGroupDetail(HTMXTemplateMixin, DetailView):
    """
    Display informations about transactions related to a same group
    and gives the ability to add and remove transactions from the group.

    The requests for association of Transactions or promotion of Postings are
    received in POST and feed to a ModelForm in charge of creating a new
    transaction or edit an existing one.
    """
    model = TransactionGroup
    context_object_name = 'group'
    template_name = 'bookkeeper/transactiongroup_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['accounts'] = self.object.transactions.get_accounts()
        try:
            context['reconciliation_account'] = self.object.check_reconciliation_account()
        except AccountingValidationError:
            context['reconciliation_account'] = None

        # build a list of transactions from the group with a deletion formset
        context['group_transactions'] = []
        for transaction in self.object.transactions.all():
            context['group_transactions'].append(
                    (transaction, TransactionGroupDeleteForm(
                        initial={'id': transaction.id}))
                )
        # build a list of pending transactions with an addition formset
        context['pending_records'] = []
        for record in Transaction.objects.search(record=True, pending=True):
            context['pending_records'].append(
                    (record, TransactionGroupAddForm(
                        initial={'id': record.id}))
                )
        # build a list of pending operations with a promote formset
        context['pending_operations'] = []
        for operation in Transaction.objects.search(operation=True, pending=True):
            context['pending_operations'].append(
                    (operation, TransactionGroupPromoteForm(
                        initial={'id': operation.id}))
                )
        return context

    def post(self, *args, **kwargs):
        """Route the proper form depending on the required action."""
        self.object = self.get_object() # required to render context data for a detail view

        if not self.request.htmx:
            return HttpResponseNotFound() # this is not supposed to happen

        try:
            # load the proper form subclass
            form_class = TransactionGroupBaseForm.get_form_class(self.request.POST)
        except KeyError:
            return HttpResponseHXError('Action not allowed')

        form = form_class(self.request.POST, instance=self.object)
        if form.is_valid():
            return self.render_to_response(self.get_context_data())
        else:
            return HttpResponseHXError(str(form.errors))



