from pathlib import PurePath
from django.views.generic.base import TemplateResponseMixin
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest


class HTMXTemplateMixin(TemplateResponseMixin):

    def get_template_names(self):
        """Return a path to a fragment for htmx instead of full page."""
        template_names = super().get_template_names()
        if self.request.htmx:
            parts = list(PurePath(template_names[0]).parts)
            parts.insert(-1, 'fragments')
            return str(PurePath('').joinpath(*parts))
        return template_names


class HttpResponseHXRedirect(HttpResponseRedirect):
    """HTMX client redirect response."""
    status_code = 200
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self["HX-Redirect"] = self["Location"]


class HttpResponseHXError(HttpResponse):
    """Adds extra headers to the response for HTMX to handle it in template."""
    status_code = 200
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self["HX-Retarget"] = "#errors p"
        self["HX-Reswap"] = "innerHTML"
