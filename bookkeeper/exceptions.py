from django.core.exceptions import ValidationError

class TransactionException(Exception):
    pass


class AccountException(Exception):
    pass


class UnbalancedTransactionException(TransactionException):
    pass


class ProtectedTransactionException(TransactionException):
    pass


class InexistingAccountException(AccountException):
    pass


class RollbackException(Exception):
    """Don't commit changes."""


## ValidationError
class AccountingValidationError(ValidationError):
    """This operation is not compliant with accounting rules."""


class BusinessValidationError(ValidationError):
    """This operation is not compliant with business rules."""
