from django import forms
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from bookkeeper.models import Budget


class BudgetCreateForm(forms.ModelForm):
    class Meta:
        model = Budget
        fields = ('type', 'name', 'objective', 'supervisor')
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'name'}),
            'objective': forms.TextInput(attrs={'placeholder': 'objective'}),
        }

class BudgetEditForm(forms.ModelForm):
    class Meta:
        model = Budget
        fields = ('name', 'objective', 'supervisor', 'status')


class BudgetFilterForm(forms.Form):

    type = forms.ChoiceField(required=False,
        choices=[(None, '-- Type --')] + Budget._meta.get_field('type').choices,
    )
    supervisor = forms.ModelChoiceField(required=False, empty_label='-- Supervisor --',
        queryset=get_user_model().objects.all(),
    )
    name = forms.CharField(required=False,
        widget=forms.TextInput(attrs={'placeholder': 'Search...'}))
    close = forms.BooleanField(required=False, label='Show closed', initial=False,
        widget=forms.CheckboxInput(attrs={'role':'switch'}))

