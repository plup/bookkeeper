from django import forms
from django.core.exceptions import ValidationError
from django.db.transaction import atomic
from bookkeeper.models import Transaction, TransactionGroup, Budget, Posting
from bookkeeper.exceptions import *


class TransactionGroupBaseForm(forms.ModelForm):
    """
    Form providing the instance object of the group
    and the ID field used for all actions regarding transaction grouping.
    """
    class Meta:
        model = TransactionGroup
        fields = ()

    # group management fields
    id = forms.CharField(required=True, widget=forms.HiddenInput())

    def save(self):
        """Nothing to do on the TransactionGroup itself."""
        pass

    @classmethod
    def get_form_class(cls, querydict):
        """
        Identify the proper TransactionGroup form subclass to use
        by extracting the prefix from the POST QueryDict.
        """
        field = next(iter(querydict)).split('-')[0]
        mapping = {
            'add': TransactionGroupAddForm,
            'delete': TransactionGroupDeleteForm,
            'promote': TransactionGroupPromoteForm,
        }
        return mapping[field]


class TransactionGroupAddForm(TransactionGroupBaseForm):
    prefix = 'add'

    def clean(self):
        """Validate the transaction to add to the group."""
        cleaned_data = super().clean()
        transaction = Transaction.objects.get(pk=cleaned_data.pop('id'))

        # check if transaction to add is available
        if transaction.group:
            raise ValidationError('This transaction is already part of another group', code='invalid')

        # add the transaction to the group (with validation)
        self.instance.add_safe(transaction)


class TransactionGroupPromoteForm(TransactionGroupBaseForm):
    prefix = 'promote'

    def clean(self):
        """Promote the temporary transaction in an operation and add it to the group."""
        cleaned_data = super().clean()
        transaction = Transaction.objects.get(pk=cleaned_data.pop('id'))

        try:
            transaction.postings.get(account__number=471)
        except:
            raise ValidationError("This operation is already reconciled", code='invalid')

        # run the validation
        try:
            reconciliation_account = self.instance.check_reconciliation_account()
        except AccountingValidationError:
            raise ValidationError('Impossible to find how to match this operation.')
        try:
            budget = self.instance.check_budget()
        except AccountingValidationError:
            raise ValidationError('This operation needs to match a budget within the group.')

        # replace the temporary account with the reconciliation account
        # and attach the transaction to the group.
        with atomic():
            transaction.promote(reconciliation_account, budget)
            self.instance.add_safe(transaction)


class TransactionGroupDeleteForm(TransactionGroupBaseForm):
    prefix = 'delete'

    def clean(self):
        """
        Remove the transaction from the group.
        If the transaction is an operation it deletes it to revert the action of the promotion.
        """
        cleaned_data = super().clean()
        transaction = Transaction.objects.get(pk=cleaned_data.pop('id'))

        # check if the transaction to remove is part of the group
        if not self.instance.transactions.filter(pk=transaction.pk).exists():
            raise ValidationError('The transaction is not part of the current group', code='invalid')

        # demote operation
        if not transaction.clearable:
            transaction.demote()

        # remove transacton from group
        self.instance.transactions.remove(transaction)
