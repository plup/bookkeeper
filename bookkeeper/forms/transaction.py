from datetime import datetime
from decimal import Decimal
from csv import reader as CSVReader
from django import forms
from django.db import transaction
from django.core.exceptions import ValidationError
from bookkeeper.helpers import TransactionType
from bookkeeper.models import Transaction, TransactionGroup, Budget, Posting, Account


class TransactionBaseForm(forms.ModelForm):
    """Base form for transactions managing the template with the common attributes."""
    class Meta:
        model = Transaction
        fields = ('date', 'payee', 'budget', 'notes', 'evidence', 'evidence_ref')
        widgets = {
            'date': forms.DateInput(attrs={'type': 'date'}),
            'payee': forms.TextInput(attrs={'placeholder': 'Payee'}),
            'evidence_ref': forms.TextInput(attrs={'placeholder': 'Reference'}),
        }

    template_name = 'bookkeeper/forms/transaction_base_form.html'

    def __init__(self, *args, **kwargs):
        """Change model fields behavior."""
        super(TransactionBaseForm, self).__init__(*args, **kwargs)
        self.fields['budget'].empty_label = '-- Budget --'
        self.fields['budget'].required = True
        self.fields['budget'].queryset = Budget.objects.filter(status='O')
        self.fields['evidence'].required = True
        self.fields['evidence_ref'].required = True

    def clean(self):
        """Basic validation for all inherited transaction forms."""
        cleaned_data = super().clean()
        if cleaned_data['budget'].status != 'O':
            raise ValidationError("Can't add or remove transaction on a closed budget.")
        return cleaned_data


class TransactionSimpleForm(TransactionBaseForm):
    """Base form for transaction involving only an amount with taxes."""
    amount = forms.FloatField(required=True, label="Amount", widget=forms.NumberInput(attrs={'placeholder': 'Amount (VAT excl.)'}))
    vat = forms.FloatField(required=True, label="VAT", widget=forms.NumberInput(attrs={'placeholder': 'VAT'}))

    template_name = 'bookkeeper/forms/transaction_simple_form.html'

    def clean(self):
        cleaned_data = super().clean()
        amount = cleaned_data.pop('amount')
        vat = cleaned_data.pop('vat')
        cleaned_data['postings'] = self._helper(amount, vat)
        return cleaned_data

    @transaction.atomic
    def save(self):
        """Save the transaction along with the postings."""
        instance = super().save(commit=True)
        instance.add_postings(self.cleaned_data['postings'])
        instance.clean() # validate model before committing
        return instance


class TransactionSaleForm(TransactionSimpleForm):
    _helper = TransactionType.project_sale


class TransactionOutsourcingForm(TransactionSimpleForm):
    _helper = TransactionType.contractor_expense


class TransactionGeneralForm(TransactionSimpleForm):
    _helper = TransactionType.internal_expense


class TransactionDefrayalForm(TransactionSimpleForm):
    _helper = TransactionType.defrayal_reimbursment


class TransactionVatExpenseForm(TransactionSimpleForm):
    _helper = TransactionType.vat_expense


class TransactionVatCreditForm(TransactionSimpleForm):
    _helper = TransactionType.vat_credit


class TransactionTaxExpenseForm(TransactionSimpleForm):
    _helper = TransactionType.tax_expense


class TransactionManualForm(TransactionBaseForm):
    """Form handling creation of any transaction."""
    template_name = 'bookkeeper/forms/transaction_manual_form.html'

    def __init__(self, *args, **kwargs):
        """Initialize the formset with its fields."""
        super().__init__(*args, **kwargs)
        formset_class = forms.inlineformset_factory(Transaction, Posting,
                fields=['account','amount'], formset=PostingFormSet, extra=1)
        try:
            # FIXME: handle passing initial parameters to formset
            del kwargs['initial']
        except KeyError: pass
        self.formset = formset_class(*args, **kwargs)
        # evidence is not mandatory in manual mode
        self.fields['evidence'].required = False
        self.fields['evidence_ref'].required = False

    def get_context(self):
        """Add the formset to the context for rendering."""
        context = super().get_context()
        context['formset'] = self.formset
        return context

    def is_valid(self):
        return self.formset.is_valid() and super().is_valid()

    def save(self):
        """Bind both models together."""
        instance = super().save(commit=True)
        self.formset.instance = instance
        self.formset.save(commit=True)
        return instance


class PostingFormSet(forms.BaseInlineFormSet):
    """Manage the posting forms in a manual transaction."""
    def _should_delete_form(self, form):
        """Remove postings with null amount or account."""
        if not form.cleaned_data.get('account') or not form.cleaned_data.get('amount'):
            return True
        return super()._should_delete_form(form)

    def clean(self):
        """Validate rules on the transaction content."""
        if any(self.errors):
            return

        accounts = 0
        total = 0
        for form in self.forms:
            # skip deleted form from validation
            if self.can_delete and self._should_delete_form(form):
                continue

            if form.cleaned_data.get('account'):
                accounts += 1

            total += form.cleaned_data.get('amount', 0)

        if not accounts:
            raise ValidationError("The transaction doesn't write in any accounts.")

        if total != 0:
            raise ValidationError(f"Credits and debits mismatch: {total}")


class TransactionTransferForm(forms.ModelForm):
    """
    Special form transfering amounts across budgets.
    It creates 1 transaction on each budget using a transfer account.
    """
    class Meta:
        model = Transaction
        fields = ('date', 'budget', 'notes')
        widgets = {
            'date': forms.DateInput(attrs={'type': 'date'}),
        }

    account = forms.ModelChoiceField(required=True, empty_label = '-- Account --',
                                     queryset=Account.objects.all())
    amount = forms.FloatField(required=True, label="Amount",
                              widget=forms.NumberInput(attrs={'placeholder': 'Amount'}))
    # add a second budget
    target = forms.ModelChoiceField(required=True, empty_label = '-- Destination --',
                                    queryset=Budget.objects.filter(status='O'))

    template_name = 'bookkeeper/forms/transaction_transfer_form.html'

    def __init__(self, *args, **kwargs):
        """Change model fields behavior."""
        super().__init__(*args, **kwargs)
        self.fields['budget'].empty_label = '-- Origin --'
        self.fields['budget'].required = True
        self.fields['budget'].queryset = Budget.objects.filter(status='O')
        self.fields['date'].initial = datetime.today()

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data['budget'] == cleaned_data['target']:
            raise ValidationError('Origin and destination are the same.')

        # set payee
        cleaned_data['payee'] = 'internal transfer'
        return cleaned_data

    @transaction.atomic
    def save(self):
        """Create the two transactions to make the transfer."""
        # create the first transaction
        instance = super().save(commit=False)
        instance.payee = self.cleaned_data['payee']
        instance.save()
        instance.add_postings(
            TransactionType.temporary_transfer(
                self.cleaned_data['amount'],
                self.cleaned_data['account'].number,
            )
        )
        # create the second transaction from the first
        opposite = instance
        opposite.pk = None
        opposite.payee = self.cleaned_data['payee']
        opposite.budget = self.cleaned_data['target']
        opposite.save()
        opposite.add_postings(
            TransactionType.temporary_transfer(
                -self.cleaned_data['amount'],
                self.cleaned_data['account'].number,
            )
        )
        # validate before committing
        instance.clean()
        opposite.clean()

        # return the transaction writing in the destination account
        return opposite


class TransactionRouterForm(forms.Form):
    """
    Form keeping the mapping between the selected form and its class.
    The form itself has no use for validation.
    """
    _mapping = {
        'sale': TransactionSaleForm,
        'outsourcing': TransactionOutsourcingForm,
        'general': TransactionGeneralForm,
        'defrayal': TransactionDefrayalForm,
        'vat_expense': TransactionVatExpenseForm,
        'vat_credit': TransactionVatCreditForm,
        'tax_expense': TransactionTaxExpenseForm,
        'transfer': TransactionTransferForm,
        'manual': TransactionManualForm,
    }

    # FIXME: build the field from the mapping to avoid repetition
    type = forms.ChoiceField(required=True, choices=(
        ('sale', 'Client sale'),
        ('outsourcing', 'Outsourcing expense'),
        ('general', 'General expense'),
        ('defrayal', 'Expense claim'),
        ('vat_expense', 'Vat expense'),
        ('vat_credit', 'Vat credit'),
        ('tax_expense', 'Tax expense'),
        ('transfer', 'Transfer between budgets'),
        ('manual', 'Manual transaction'),
    ))

    @classmethod
    def get_form_class(cls, _type):
        try:
            return cls._mapping[_type]
        except KeyError:
            return TransactionManualForm


class TransactionSearchForm(forms.Form):
    """Transaction search form."""
    template_name = 'bookkeeper/forms/transaction_search_form.html'

    search = forms.CharField(required=False,
        widget=forms.TextInput(attrs={'placeholder': 'Search...'}))
    budget = forms.ChoiceField(required=False, choices=[('','')])
    year = forms.ChoiceField(required=False, choices=[('','')])
    pending = forms.BooleanField(required=False, label='Pending', initial=False,
        widget=forms.CheckboxInput(attrs={'role':'switch'}))
    proof = forms.BooleanField(required=False, label='Need proof', initial=False,
        widget=forms.CheckboxInput(attrs={'role':'switch'}))
    group = forms.BooleanField(required=False, label='Regroup', initial=False,
        widget=forms.CheckboxInput(attrs={'role':'switch'}))
    record = forms.BooleanField(required=False, label='Record only', initial=False,
        widget=forms.CheckboxInput(attrs={'role':'switch'}))
    operation = forms.BooleanField(required=False, label='Operation only', initial=False,
        widget=forms.CheckboxInput(attrs={'role':'switch'}))


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['budget'].choices = [(None, '-- Budget --'),] + [(t.code, t) for t in Budget.objects.all()]
        self.fields['year'].choices = [(None, '-- Year --'),] + [(d.year, d.year) for d in Transaction.objects.dates('date', 'year', order='DESC')]


class TransactionImportForm(forms.Form):
    """
    The transaction import form loads all the data from a CSV file.
    It bulk creates the corresponding transaction after validation of the whole dataset.

    """
    file = forms.FileField(required=True, label='CSV file containing 1 column for date, 1 column for value date (unused), 1 column for the amount, 1 column for the label and 1 column for the resulting account balance. All columns are separated by tabulations with dots for decimal seprators, dates formatted in DD/MM/YYYY and negative debits')
    account = forms.ModelChoiceField(required=True, queryset=Account.objects.filter(number=512))

    def clean_file(self):
        """Convert the file from ISO-8859 with CRLF line terminators to UTF-8."""
        return self.cleaned_data['file'].read().decode('iso-8859-1').splitlines()

    def clean(self):
        """
        Extract transactions from the file.

        The statement is an extract of the accounts of an external source
        so the debits and credits are mirrored.
        But it also generally uses the opposite convention for debits and
        credits signs (negative debits)
        """
        cleaned_data = super().clean()
        last_balance = cleaned_data['account'].balance or 0
        # extract the transactions
        cleaned_data['transactions'] = []
        csv = cleaned_data['file']
        reader = CSVReader(csv, delimiter='\t')
        next(reader)
        for row in reader:
            try:
                # convert data from csv
                transaction = {
                    'date': datetime.strptime(row[0], '%d/%m/%Y').date(),
                    'amount': Decimal(row[2].replace(',','.')),
                    'payee': row[3],
                    'balance': Decimal(row[4].replace(',','.'))
                }

                # skip transactions already in the account
                last_transaction = (self.cleaned_data['account']
                                    .transactions.order_by('date').last())
                if last_transaction and transaction['date'] <= last_transaction.date:
                    continue

                # FIXME: disabling the balance checking as legacy data are not clean in database
                # calculate the new balance and compare it with the given one
                #mismatch = transaction['balance'] - last_balance - transaction['amount']
                #last_balance = given_balance
                #if mismatch:
                #    raise ValidationError(f'This import has a balance mismatch with the account: {mismatch}')

                # store the transaction
                cleaned_data['transactions'].append(transaction)

            except ValueError as e:
                raise ValidationError(e)

        return cleaned_data

    @transaction.atomic
    def save(self):
        """Creates the transactions."""
        # FIXME: we could beneficiate from an overriden "bulk_creates" taking
        # care of triggering the post saves actions after the import
        for t in self.cleaned_data['transactions']:
            Transaction.objects.create(
                date = t['date'],
                payee = t['payee'],
                postings = TransactionType.bank_waiting(t['amount'])
            )
