from datetime import datetime
from django import forms
from django.core.exceptions import ValidationError
from csv import reader
from bookkeeper.models import Transaction, Posting


class AccountFilterForm(forms.Form):
    """Account filters."""
    year = forms.ChoiceField(required=False, choices=[('','')])
    stack = forms.BooleanField(required=False, label='Cumulative', initial=False,
        widget=forms.CheckboxInput(attrs={'role':'switch'}))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['year'].choices = [(None, '-- Year --'),] + [(d.year, d.year) for d in Transaction.objects.dates('date', 'year', order='DESC')]
