class TransactionType(object):
    """Helpers returning predefined set of postings."""
    @classmethod
    def project_sale(self, amount, vat):
        """
        Helper creating a project sale.
        Tracking the business providing part in the appropriate account
        """
        return [
            (706, -amount), # credit sales account
            (4457, -vat),         # credit collected taxes
            (411, amount + vat),  # debit clients account
        ]

    @classmethod
    def contractor_expense(self, amount, vat):
        """Helper creating a purchase from a contractor."""
        return [
            (401, -amount - vat), # credit contractor account of total
            (4456, vat) ,         # debit paid taxes
            (604, amount),        # debit service account
        ]

    @classmethod
    def internal_expense(self, amount, vat):
        """Helper creating a purchase from a contractor for an internal service."""
        return [
            (401, -amount - vat), # credit contractor account of total
            (4456, vat) ,         # debit paid taxes
            (611, amount),        # debit general service account
        ]

    @classmethod
    def defrayal_reimbursment(self, amount, vat):
        """Helper recording a payback for a board member."""
        return [
            (455, -amount - vat), # credit associate account
            (4456, vat) ,         # debit paid taxes
            (625, amount),        # debit general expense account
        ]

    @classmethod
    def receive_loan(self, amount, vat=0):
        """Helper recording the amount given by a bank as a loan."""
        return [
            (164, -amount),     # credit the amount received
            (512, amount),      # debit the bank
        ]

    @classmethod
    def vat_expense(self, amount, vat=0):
        """
        Helper recording the amount of taxes to be paid.
        The given amount is already the result of the difference
        between taxes collected and taxes to deduce.
        """
        return [
            (4457, amount),     # debit the amount of taxes collected
            (44551, -amount),   # credit the account tracking taxes paid
        ]

    @classmethod
    def vat_credit(self, amount, vat=0):
        """
        Helper recording the amount of VAT to receive or keep as credit.
        The given amount is already the result of the difference
        between taxes collected and taxes to deduce.
        """
        return [
            (4456, -amount),   # credit the amount of taxes to deduce
            (44567, amount),   # debit the account tracking taxes reimbursment
        ]

    @classmethod
    def tax_expense(self, amount, vat=0):
        """Helper recording taxes other than vat and profit."""
        return [
            (6351, amount),     # debit expense account
            (447, -amount),     # credit liability account for taxes
        ]

    @classmethod
    def engaged_expense(self, amount, vat=0):
        """Helper recording expenses to come."""
        # FIXME doesn't handle vat
        return [
            (604, amount),      # debit service account
            (408, -amount),     # credit unreceived expense account
        ]

    @classmethod
    def bank_waiting(self, amount, vat=0):
        """Helper creating a record from bank in temporary account."""
        return [
            (512, amount),
            (471, -amount),
        ]

    @classmethod
    def temporary_transfer(self, amount, account):
        """Helper debiting the given account and crediting temporary account."""
        return [
            (account, amount),
            (478, -amount),
        ]

