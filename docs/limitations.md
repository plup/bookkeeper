# Known limitations

* The `need proof` filter only searches in `clearable` transactions. By choice, it doesn't show the `autoclearing` transactions (directly recordedin bank).
