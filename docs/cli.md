# CLI commands

## Bookkeeper commands

### Export transactions for cutoff

```
$ python manage.py export --from 2022-01-01 --to 2022-12-31 transactions_2022.csv
```

### Automatic reconciliation

Troubleshoot matching by reference from debug logs:
```
# python manage.py reconcile 2>&1 | grep -A1 0032429
attempting to match record 10890 on budget P347 with evidence VTE/0032429...
record 10890 not matched

>>> Transaction.objects.search(operation=True, pending=True).filter(payee__icontains='0032429')
<TransactionQuerySet [<Transaction: #24318 2022-08-01 VIR VTE/0032429-HD347>]>
```

Monitor specific rules triggering:
```
# python manage.py reconcile 2>&1 | grep 'affected'
# python manage.py reconcile 2>&1 | grep 'changed'
```

## Django shell

Check for non balanced group:
```
from bookkeeper.models import *
from bookkeeper.exceptions import *
for group in TransactionGroup.objects.all():
    try:
        if group.check_reconciliation_account().balance != 0:
            print(f'GID {group.id}, {group.check_reconciliation_account().balance}')
    except AccountingValidationError as e:
        pass
```

Manually create an operation:
```
from datetime import date
from bookkeeper.helpers import TransactionType
from bookkeeper.models import *
Transaction.objects.create(date=date(2023,10,6),payee='SOMEBODY',postings=TransactionType.bank_waiting(1008))
```

Change postings from an operation:
```
from bookkeeper.models import *
t = Transaction.objects.get(pk=7801)
t.postings.all()
<QuerySet [<Posting: 3600.00€ in banques [512]>, <Posting: -3600.00€ in clients [411]>]>

t.postings.filter(account__number=411).update(amount=-900)
t.postings.filter(account__number=512).update(amount=900)
t.clean()

t.postings.all()
<QuerySet [<Posting: 900.00€ in banques [512]>, <Posting: -900.00€ in clients [411]>]>
```

Group operations together (prefer using an existing group on the same budget):
```
from bookkeeper.models import *
Transaction.objects.search(search='chaudron').pending().get_accounts().values('number', 'balance')
<AccountQuerySet [{'number': 512, 'balance': Decimal('0.00')}, {'number': 471, 'balance': Decimal('0.00')}]>


group = TransactionGroup.objects.create()
Transaction.objects.search(search='chaudron').pending().update(group=group)
group.transactions.get_accounts().values('number', 'balance')
<AccountQuerySet [{'number': 512, 'balance': Decimal('0.00')}, {'number': 471, 'balance': Decimal('0.00')}]>
```
