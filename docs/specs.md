# Specification for bookkeeper module

## Abstract

`bookkeeper` is a simple accounting module compliant with double-entry bookkeeping rules:

 1. Every financial category in the business is represented by an **account**.
 2. Every financial **transaction** in the business can be represented as a transfer between accounts.
 3. A **transaction** must always balance (the sum of the transfers must equal 0)

Those as the root accounts commonly used:

 * Assets: Anything you own
 * Liabilities: Anything you owe
 * Revenue/income: What your business earns
 * Expenses: The cost of doing business
 * Equity: Reflects your current ownership level

By convention, a debit entry will increase the balance of both asset and expense accounts, while a credit entry will increase the balance of liabilities, revenue, and equity accounts.

## Business logic

### Accounting rules

The transaction keeps track of multiple transfers between accounts. They occur at a specific date and it SHOULD be possible to attach them notes.

And an account appears in multiple transactions. The same account MUST only appear once in a single transaction. Accounts have names and numbers to identify them.

The accounts are predefined by the accounting plan and therefor CAN be static values. But the transactions and their records MUST be dynamical data.

It MUST be possible to state the balance of each account.

Also, empty transactions are ok from an accouting point of view (though it doesn't make much sense) but transfers CANNOT exist without a transaction to hold them.

### Business transactions

The business transactions to handle are:

 * Selling a project to a client
 * Buying a service to contractor
 * Recording defrayals for associates
 * Paying services like emails, chat,...
 * Paying taxes to the state
 * Making amend to project to handle special cases

## Data model

### Entities description

3 entities can be defined form the business logic:

 * the `accounts` representing the source and destination of transfers
 * the `postings` keeping tracks of the amount of money going in or out an `account`
 * the `transactions` holding the `postings` relative to a same financial operation

### Relational model

Relational model for `transactions` and `accounts` through `postings`:
```
                        POSTING
                      ┌─────────────┐
                      │ amount      │
                      │ account ────┼────► ACCOUNT
   TRANSACTION   ◄────┤ transaction │    ┌─────────┐
 ┌───────────────┐    └─────────────┘    │ number  │
 │ date          │                       │ name    │
 │ notes         │                       └─────────┘
 └───────────────┘
```

### Transaction types

Transaction type defined by their accounts reconciliation status:

 * (True, True)   -> external operation (ex: bank to bank)
 * (True, False)  -> normal operation (ex: bank to client)
 * (True, None)   -> pending operation (ex: bank to temporary account)
 * (False, False) -> liability transfer record (prohibited by transaction contrainst)
 * (False, None)  -> normal record (ex: client to income)
 * (None, None)   -> internal transaction (ex: transfer between budgets)

## Data validation

* Transaction MUST NOT write in both product and expense accounts
* Transaction MUST NOT write in both client and provider accounts
