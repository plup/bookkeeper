# Comptabilité

## Workflow

### Vente

Saisie d'une écriture de vente:

| Compte | Tiers  | Débit | Crédit |
|--------|--------|-------|--------|
| 411    | client | 1200  |        |
| 706    | client |       | -1000  |
| 4457   | client |       | -200   |

Rapprochement du paiement correspondant:

| Compte | Tiers  | Débit | Crédit |
|--------|--------|-------|--------|
| 411    | client |       | -1200  |
| 512    | client | 1200  |        |

### Achat

Saisie d'une écriture d'achat:

| Compte | Tiers  | Débit | Crédit |
|--------|--------|-------|--------|
| 401    | presta |       | -1200  |
| 604    | presta | 1000  |        |
| 4456   | presta | 200   |        |

Rapprochement du paiement correspondant:

| Compte | Tiers  | Débit | Crédit |
|--------|--------|-------|--------|
| 401    | presta | 1200  |        |
| 512    | presta |       | -1200  |

Lors du rapprochement il faut faire correspondre les montants de la banque avec ceux des comptes de charges.

Attention, une ligne en crédit de la banque correspond à une ligne en débit pour l'entreprise (elle crédite le compte de l'entreprise dans sa propre comptabilité ce qui fait augmenter sa dette).

## Opérations comptables manuelles

### Clore un exercice

1. Réconciliées les transactions de l'année concernée
2. Générer les FNP/FAE théorique sur les projets
3. Rectifier les FNP/FAE en fonction des retours des capitaines
4. Exporter les transactions de l'année concernée avec les FNP/FAE pour le comptable

### Enregistrer le déblocage d'un prêt

Le déblocage du prêt figure dans les écritures du relevé et bancaire. Il est affecter au compte d'attente après import:
```
>>> Transaction.objects.filter(payee='DEBLOCAGE PRET 06082 205537 02').first().postings.all()
<QuerySet [<Posting: 2880.00€ in banques [512]>, <Posting: -2880.00€ in compte d'attente [471]>]>
```

Il faut transformer ces opérations en réception d'emprunt en remplaçant le compte `471` par le compte `164`:
```
>>> Transaction.objects.filter(payee='DEBLOCAGE PRET 06082 205537 02').first().postings.all()
<QuerySet [<Posting: 2880.00€ in banques [512]>, <Posting: -2880.00€ in emprunts [164]>]>
```

### Transactions analytiques

#### Rééquilibrer un projet en déficit

1. Toutes les transactions doivent avoir été réconciliées (éventuellement avec un avoir)
2. Accepter la perte en sortant une partie des charges un impayé:
    * Débiter le compte 6041 "Pertes en litige"
    * Créditer le compte de charge lié, exemple: 604 "Prestation de service"

Si la transaction en litige a fait l'objet d'une **décision de justice** alors il faut débiter le compte 6714 "Créances irrécouvrables" pour enregistrer l'impayé et faire une demande de remboursement de la TVA.


#### Rééquilibrer un projet en excès

1. Toutes les transactions doivent avoir été réconciliées
2. Valider l'excédant en enregistrant un produit exceptionnel:
    * Débiter le compte de produit lié, exemple: 7061 "Projets numériques"
    * Créditer le compte 778 "Autres produits exceptionnels"
