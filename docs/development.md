# Notes for developers

## Import production db

Reset the local project to the production codebase:
```
$ docker compose down -v
$ git checkout main
$ docker compose up -d
```

Import the production data locally:
```
$ export SERVER=...
$ ssh $SERVER "\$HOME/bookkeeper/venv/bin/python \$HOME/bookkeeper/manage.py dumpdata --natural-foreign --natural-primary auth.user bookkeeper > /tmp/bk.json" && scp $SERVER:/tmp/bk.json /tmp/bk.json

$ docker compose cp /tmp/bk.json web:/tmp/bk.json && docker compose exec web python manage.py loaddata /tmp/bk.json
```

## Validate data migration

After restoring the production codebase and data, switch to the development code and upgrade:
```
$ git checkout release/...
$ docker compose exec web python manage.py migrate
```

## Post migration controls

Check for empty groups:
```
from bookkeeper.models import *
from bookkeeper.exceptions import *
group in TransactionGroup.objects.all():
    if not group.transactions.exists():
        print(f'GID {group.id}')
```

Check for non balanced group:
```
from bookkeeper.models import *
from bookkeeper.exceptions import *
for group in TransactionGroup.objects.all():
    try:
        if group.check_reconciliation_account().balance != 0:
            print(f'GID {group.id}, {group.check_reconciliation_account().balance}')
    except AccountingValidationError as e:
        pass
```
